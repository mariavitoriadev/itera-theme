<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W74JPNM');</script>
	<!-- End Google Tag Manager -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
<script type="text/javascript">(function(n,r,l,d){try{var h=r.head||r.getElementsByTagName("head")[0],s=r.createElement("script");s.setAttribute("type","text/javascript");s.setAttribute("src",l);n.neuroleadId=d;h.appendChild(s);}catch(e){}})(window,document,"https://cdn.neurologic.com.br/neurolead/neurolead.min.js", 6777);</script>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W74JPNM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<header class="header">
		<div class="header-wrapper container">
			<div class="menu-button mobile" [class]="header.menu == false ? 'menu-button' : 'menu-button hidden'" on="tap:AMP.setState({header: {menu: true}})" tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/menu.svg') ?>
			</div>
			<div class="menu-button close hidden mobile" [class]="header.menu == false ? 'menu-button close hidden' : 'menu-button close'" on="tap:AMP.setState({header: {menu: false}})" tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/menu-close.svg') ?>
			</div>
			<div class="logo">
				<div class="logo">
					<?= is_front_page() ? '<h1>' : '' ?>
					<a href="<?= get_home_url()  ?>">
						<?= get_theme_mod('logo-svg') ?>
					</a>
					<?= is_front_page() ? '</h1>' : '' ?>
				</div>
			</div>
			<div class="contact-button mobile"tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/phone.svg') ?>
			</div>
			<div class="menu-main-wrapper desktop">
				<div class="menu-main">
					<?php wp_nav_menu(array('theme_location' => 'header-main')) ?>
				</div>
				<a class="header-cta desktop" href="<?= get_theme_mod('button-url') ?>"><?= get_theme_mod('button-title') ?></a>
			</div>
		</div>
		<?php if( !get_theme_mod('alternative-header') ): ?>
		<div class="menu-open hidden">
			<div class="container menu-nav mobile">
				<?php wp_nav_menu(array('theme_location' => 'header-mobile')) ?>
			</div>
			<div class="solutions-menu">
				<div class="header-card desktop">
					<a href="<?= get_theme_mod('header-card-cta'); ?>">
                        <img src="<?= get_theme_mod('header-card-image'); ?>">
                        <div class="header-card-content">
                            <p class="widget-popular-post-title"><?= get_theme_mod('header-card-title'); ?></p>
                            <p class="read-more"><?= get_theme_mod('header-card-cta-text'); ?></>
                        </div>
					</a>
				</div>
				<a href="/solucoes/" class="alice-card">
					<div>
						<p class="title mobile">Soluções</p>
						<p class="description">Conheça ALICE, nossa plataforma de Inteligência Artificial</p>
						<span class="read-more">Saiba mais</span>
					</div>
				</a>
			</div>
			<div class="solutions-items">
				<p class="title">Soluções por setor</p>
				<div class="solutions-items-wrapper">
					<?php
					$solutions = get_terms(array( 'hide_empty' => false, 'taxonomy' =>'solution-category'));
					foreach ($solutions as $solution): ?>
						<a class="solution-item" href="<?= get_term_link($solution->term_id) ?>"><?= $solution->name ?></a>
					<?php endforeach;?>
				</div>
			</div>
			<div class="cta-menu mobile">
				<a class="header-cta" href="<?= get_theme_mod('button-url') ?>"><?= get_theme_mod('button-title') ?></a>
			</div>
		</div>
		<?php else: ?>
			<div class="menu-open alternative hidden">
			<div class="container menu-nav mobile">
				<?php wp_nav_menu(array('theme_location' => 'header-mobile')) ?>
			</div>
			<div class="solutions-menu">
				<div class="header-card">
					<a href="<?= get_theme_mod('header-card-cta'); ?>">
                        <img src="<?= get_theme_mod('header-card-image'); ?>">
                        <div class="header-card-content">
							<p class="sector"><?= get_theme_mod('header-card-sector'); ?></p>
							<p class="widget-popular-post-title"><?= get_theme_mod('header-card-title'); ?></p>
                            <p class="read-more"><?= get_theme_mod('header-card-cta-text'); ?></p>
                        </div>
					</a>
				</div>
				<a href="/solucoes/" class="alice-card">
					<div>
						<p class="sector">Inteligência artifical</p>
						<p class="description">Conheça ALICE, nossa plataforma de Inteligência Artificial</p>
						<span class="read-more">Saiba mais</span>
					</div>
				</a>
				<div class="header-card desktop">
					<a href="<?= get_theme_mod('header-card-2-cta'); ?>">
                        <img src="<?= get_theme_mod('header-card-2-image'); ?>">
                        <div class="header-card-content">
							<p class="sector"><?= get_theme_mod('header-card-2-sector'); ?></p>
                            <p class="widget-popular-post-title"><?= get_theme_mod('header-card-2-title'); ?></p>
                            <p class="read-more"><?= get_theme_mod('header-card-2-cta-text'); ?></p>
                        </div>
					</a>
				</div>
			</div>
			<div class="cta-menu mobile">
				<a class="header-cta" href="<?= get_theme_mod('button-url') ?>"><?= get_theme_mod('button-title') ?></a>
			</div>
		</div>
		<?php endif; ?>
		<div class="header-background">
		</div>
	</header>