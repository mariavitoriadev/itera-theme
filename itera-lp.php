<?php /* Template Name: Itera LP */ ?>
<?php get_header('lp'); ?>
<main class="itera-lp">
    <section class="lp-hero">
        <span class="hero-layers"><?= file_get_contents(get_template_directory().'/images/lp-hero-layers.svg') ?></span>
        <div class="hero-content container">
            <p class="hero-sub-title"><?= get_field('hero_sub_title')?></p>
            <?php 
            $title_words = explode(',' , get_field('hero_title')); 
                foreach ($title_words as $index => $word) : ?>
                    <p class="hero-title <?= $index == 0 ? 'selected' : '' ?> " id="word-<?= $index ?>"><?= $word ?></p>
            <?php endforeach; ?>
            <div class="hero-description"><?= get_field('hero_description')?></div>
        </div>
        <p class="section-down">Saiba como isso é possivel</p>
        <p class="hero-cta open-popup"><?= get_field('header_cta')['title'] ?></p>
    </section>

    <section class="section-1">
        <span class="section-down-target" id="<?= get_field('section1_id') ?>"></span>
        <div class="section-image container">
            <div class="swiper-wrapper">
            <?php
                if( have_rows('section1_images') ):
                    while( have_rows('section1_images') ) : 
                    the_row(); ?>
                     <div class="swiper-slide">
                        <img src="<?= get_sub_field('section1_image')['url'] ?>">
                    </div>
                    <?php endwhile; 
                endif; ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <span class="section-background"></span>
        </div>
        <div class="section-header minor-container">
            <p class="section-tab"><?= get_field('section1_tab') ?></p>
            <p class="section-title"><?= get_field('section1_title') ?></p>
            <p class="section-description"><?= get_field('section1_description') ?></p>
        </div>
        <div class="section-icons minor-container">
        <?php
            if( have_rows('section1_items') ):
                while( have_rows('section1_items') ) : 
                the_row(); ?>

                    <?php
                        $item_versions = '';
                        if( have_rows('page_versions') ):
                            while( have_rows('page_versions') ) : the_row(); 
                           
                            $item_versions .= ' page-version-' . get_sub_field('version');
                        endwhile; 
                    endif; ?>

                <div class="item-icon <?= $item_versions ?> ">
                <?php if(get_sub_field('section1_item_svg') != ''): ?>
                        <span class="icon"><?= get_sub_field('section1_item_svg') ?></span>
                <?php else: ?>
                        <img class="icon" src="<?= get_sub_field('section1_item_icon')['url'] ?>">
                <?php endif; ?>
                    <p class="text"><?= get_sub_field('section1_item_text') ?></p>
                </div>
                <?php endwhile; 
            endif; ?>
        </div>
    </section>
    
    <section class="section-2">
        <span class="section-down-target" id="<?= get_field('section2_id') ?>"></span>
        <div class="section-header minor-container">
            <p class="section-tab"><?= get_field('section2_tab') ?></p>
            <p class="section-title"><?= get_field('section2_title') ?></p>
            <p class="section-description"><?= get_field('section2_description') ?></p>
        </div>
        <div class="images-section">
            <div class="left section-side">
                <div class="section-wrapper container">
                    <p class="title">ENSINE A ALICE COMO VOCÊ INTERPRETA AS DIFERENTES FORMAS DE DIZER A MESMA COISA.</p>
                    <span class="image"><?= file_get_contents(get_template_directory().'/images/lp-text-list.svg') ?></span>
                </div>
            </div>
            <div class="right section-side">
 
                <svg width="242" height="10" viewBox="0 0 242 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M241.5 5C241.5 5 140.654 5 0.5 5" stroke="#F7F7F7" stroke-opacity="0.3" stroke-width="10" style="mix-blend-mode:soft-light"/>
                    <circle class="alice-circle" cx="156" cy="5" r="5" fill="white"/>
                    <style>
                        @keyframes alice-circle {
                            from {transform: translateX(-120px); opacity: 0}
                            to {transform: translateX(0); opacity: 1}
                        }
                        .alice-circle {  animation: alice-circle 3s cubic-bezier(.4,.2,.2,1) infinite}
                    </style>
                </svg>

                <div class="section-wrapper container">
                    <p class="title">A ALICE APRENDE E SEMPRE TE ENTREGA O RESULTADO COMO VOCÊ DECIDIU.</p>
                        <img src="<?= get_template_directory_uri() . '/images/lp-side-image.png' ?>" >
                </div>
            </div>
        </div>
        <div class="section-icons minor-container">
            <?php
                if( have_rows('section2_items') ):
                    while( have_rows('section2_items') ) : 
                    the_row(); ?>

                    <?php
                        $item_versions = '';
                        if( have_rows('page_versions') ):
                            while( have_rows('page_versions') ) : the_row(); 
                           
                            $item_versions .= ' page-version-' . get_sub_field('version');
                        endwhile; 
                    endif; ?>

                    <div class="item-icon <?= $item_versions ?> ">
                        <img class="icon" src="<?= get_sub_field('section2_item_icon')['url'] ?>">
                        <p class="text"><?= get_sub_field('section2_item_text') ?></p>
                    </div>
                <?php endwhile; 
            endif; ?>
        </div>
    </section>

    <section class="section-3">
        <span class="section-down-target" id="<?= get_field('section3_id') ?>"></span>
        <div class="section-header minor-container">
            <p class="section-tab"><?= get_field('section3_tab') ?></p>
            <p class="section-title"><?= get_field('section3_title') ?></p>
            <p class="section-description"><?= get_field('section3_description') ?></p>
        </div>
        <div class="images-section">
            <div class="left section-side">
                <div class="section-wrapper container">
                    <p class="title">importe E ENVIE DOCUMENTOS AUTOMATICAMENTe de fontes internas ou externas COM INTEGRAÇÃO POR API.</p>
                    <span class="image"><?= file_get_contents(get_template_directory().'/images/lp-codes.svg') ?></span>
                </div>
            </div>
            <div class="right section-side">
                <div class="section-wrapper container">
                    <p class="title">exporte RAPIDAMENTE OS DADOS PLANILHADOS PARA SEU AMBIENTE DE ANÁLISES OU PARA SEU MOTOR DE CRÉDITO.</p>
                    <span class="image"><?= file_get_contents(get_template_directory().'/images/lp-excel-icons.svg') ?></span>
                </div>
            </div>
        </div>
        <p class="minor-container items-pre-text"><?= get_field('section3_pre_text') ?></p>
        <div class="section-icons minor-container">
            <?php
                if( have_rows('section3_items') ):
                    while( have_rows('section3_items') ) : 
                    the_row(); ?>

                    <?php
                        $item_versions = '';
                        if( have_rows('page_versions') ):
                            while( have_rows('page_versions') ) : the_row(); 
                           
                            $item_versions .= ' page-version-' . get_sub_field('version');
                        endwhile; 
                    endif; ?>

                    <div class="item-icon <?= $item_versions ?> ">
                        <img class="icon" src="<?= get_sub_field('section3_item_icon')['url'] ?>">
                        <p class="text"><?= get_sub_field('section3_item_text') ?></p>
                    </div>
                <?php endwhile; 
            endif; ?>
        </div>
    </section>

    <section class="section-4 container">
        <div class="section-image">
            <img src="<?= get_field('section4_image')['url'] ?>">
        </div>
        <div class="section-header minor-container">
            <p class="section-tab"><?= get_field('section4_tab') ?></p>
            <p class="section-title"><?= get_field('section4_title') ?></p>
            <p class="section-description"><?= get_field('section4_description') ?></p>
        </div>
    </section>

    <?php 
    $current_id = get_the_ID();
    $frontpage_id = get_option( 'page_on_front' );
    $home_query = new WP_Query(array('page_id' => $frontpage_id));
    while($home_query->have_posts()):
        $home_query->the_post();?>

        <section class="companies-section">
            <div class="section-header minor-container">
                <h2 class="section-title"><?= get_field('section_partners_title', $current_id) ?></h2>
            </div>
            <div class="companies-partners container">
                <div class="swiper-wrapper">
                <?php
                    if( have_rows('partners') ):
                        while( have_rows('partners') ) : 
                            the_row();
                            $url = get_sub_field('partner_logo')['sizes']['medium']; ?>
                            <div class="swiper-slide">
                                <img src="<?= $url ?>">	
                            </div>
                        <?php endwhile; 
                    endif;
                ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="companies-colabs minor-container">
                <?php
                    if( have_rows('colabs') ):
                        while( have_rows('colabs') ) : 
                            the_row();
                            $url = get_sub_field('colab_logo')['sizes']['medium']; ?>
                            <img src="<?= $url ?>">	
                        <?php endwhile; 
                    endif;
                ?>
            </div>
        </section>

    <?php endwhile;
    wp_reset_postdata();
    wp_reset_query();
    ?> 

    <section class="section-5">
        <span class="section-down-target" id="<?= get_field('section5_id') ?>"></span>
        <div class="section-header minor-container">
            <p class="section-tab"><?= get_field('section5_tab') ?></p>
            <p class="section-title"><?= get_field('section5_title') ?></p>
            <p class="section-description"><?= get_field('section5_description') ?></p>
        </div>
        <div class="images-section">
            <div class="left section-side">
                <div class="section-wrapper container">
                    <p class="title"><?= get_field('section5_left_title') ?></p>
                    <div class="section-icons">
                        <?php
                        if( have_rows('section5_left_items') ):
                            while( have_rows('section5_left_items') ) : 
                            the_row(); ?>

                            <?php
                                $item_versions = '';
                                if( have_rows('page_versions') ):
                                    while( have_rows('page_versions') ) : the_row(); 
                                
                                    $item_versions .= ' page-version-' . get_sub_field('version');
                                endwhile; 
                            endif; ?>

                            <div class="item-icon <?= $item_versions ?> ">
                                <img class="icon" src="<?= get_sub_field('section5_item_icon') ?>">
                                <p class="text"><?= get_sub_field('section5_item_text') ?></p>
                            </div>
                            <?php endwhile; 
                        endif; ?>
                    </div>
                </div>
            </div>
            <div class="right section-side">
                <div class="section-wrapper container">
                    <p class="title"><?= get_field('section5_right_title') ?></p>
                    <div class="section-icons">
                        <?php
                        if( have_rows('section5_right_items') ):
                            while( have_rows('section5_right_items') ) : 
                            the_row(); ?>

                            <?php
                                $item_versions = '';
                                if( have_rows('page_versions') ):
                                    while( have_rows('page_versions') ) : the_row(); 
                                
                                    $item_versions .= ' page-version-' . get_sub_field('version');
                                endwhile; 
                            endif; ?>

                            <div class="item-icon <?= $item_versions ?> ">
                                <img class="icon" src="<?= get_sub_field('section5_item_icon') ?>">
                                <p class="text"><?= get_sub_field('section5_item_text') ?></p>
                            </div>
                            <?php endwhile; 
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="faq minor-container">
        <p class="section-title"><?= get_field('faq_title') ?></p>
        <div class="faq-items">
            <?php
                if( have_rows('faq_itens') ):
                    while( have_rows('faq_itens') ) : 
                        the_row(); ?>
                        <div class="faq-item">
                            <p class="faq-question"><?= get_sub_field('faq_question') ?></p>
                            <div class="faq-answer">
                                <p><?= get_sub_field('faq_answer') ?></p>
                            </div>
                        </div>
                    <?php endwhile; 
                endif;
            ?>     
        </div>
    </section>
</main>


<div class="download-popup container hidden" id="download-popup">
    <script>
        var redirectPage = '<?= get_field('popup_redirect')['url'] ?>'
    </script>
    <div class="popup-image"><?= get_the_post_thumbnail() ?></div>
    <div class="popup-content">
        <p class="tab"><?= get_field('popup_tab') ?></p>
        <p class="title"><?= get_field('popup_title') ?></p>
        <p class="description"><?= get_field('popup_description') ?></p>
        <form
        id="popup-form"
        class="download-popup-form newsletter-widget"
        method="POST"
        action="<?= site_url() ?>/wp-admin/admin-ajax.php?action=insert_lead"
        >   
        <div class="newsletter-form-wrapper">
            <label class="newsletter-widget-label">
                <input name="name" type="text" placeholder="Seu nome*" id="popup-name" class="newsletter-widget-email">
                <span class="contact-form-alert-msg name hide">* Campo Obrigatório</span>
            </label>
            <label class="newsletter-widget-label">
                <input name="email" type="email" placeholder="Email corporativo*" id="popup-email" class="newsletter-widget-email">
                <span class="contact-form-alert-msg email hide">* Campo Obrigatório</span>
                <span class="newsletter-widget-alert-msg hide">* E-mail Inválido</span>
            </label>
            <label class="newsletter-widget-label">
                <input name="company-name" type="text" placeholder="Nome da empresa" id="popup-company-name" class="newsletter-widget-email">
                <span class="contact-form-alert-msg company-name hide">* Campo Obrigatório</span>
            </label>
            <label class="newsletter-widget-label select-input">
                <select name="company-sector" id="popup-company-sector" class="newsletter-widget-email">
                    <option disabled selected >Qual o setor da sua empresa?</option>
                    <?php
                        if( have_rows('company_sector') ):
                            while( have_rows('company_sector') ) : 
                                the_row(); ?>
                                <option value="<?= get_sub_field('sector_tag') ?>"><?= get_sub_field('sector_item') ?></option>
                            <?php endwhile; 
                        endif;
                    ?>     
                </select>
                <span class="contact-form-alert-msg company-sector hide">* Campo Obrigatório</span>
            </label>
            <label class="newsletter-widget-label select-input">
                <select name="company-function" id="popup-company-function" class="newsletter-widget-email">
                    <option disabled selected >Qual a sua função?</option>
                    <?php
                        if( have_rows('company_function') ):
                            while( have_rows('company_function') ) : 
                                the_row(); ?>
                                <option value="<?= get_sub_field('function_tag') ?>"><?= get_sub_field('function_item') ?></option>
                            <?php endwhile; 
                        endif;
                    ?>     
                </select>
                <span class="contact-form-alert-msg company-function hide">* Campo Obrigatório</span>
            </label>
            <div class="input-checkbox">
                <input type="checkbox" id="more-news" name="more-news" value="receive">
                <span></span>
                <label for="more-news">Quero receber novidades ITERA para meu setor.</label>
            </div>

            <input type="hidden" name="nonce" value="<?= wp_create_nonce('insert_lead') ?>">
            <input type="hidden" class="list-id" name="list-id" value="3">
            <input class="newsletter-widget-submit" type="submit" value="Quero uma demonstração">
        </div>              
        </form>
    </div>
    <span class="close-popup">
        <?= file_get_contents( get_template_directory().'/images/menu-close.svg') ?>
    </span>
</div>
<div class="popup-overlay hidden"></div>

<?php get_footer(); ?>
