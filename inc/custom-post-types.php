<?php 

function custom_post_type() {

	$labels = array(
		'name'                  => 'Soluções',
		'singular_name'         => 'Solução',
		'menu_name'             => 'Soluções' ,
		'name_admin_bar'        => 'Soluções' ,
		'attributes'            => 'Atributos' ,
		'all_items'             => 'Todos as Soluções' ,
		'add_new_item'          => 'Adicionar Solução' ,
		'add_new'               => 'Adicionar' ,
		'new_item'              => 'Nova Solução' ,
		'edit_item'             => 'Editar Solução' ,
		'update_item'           => 'Atualizar Solução' ,
		'view_item'             => 'Ver Solução' ,
		'view_items'            => 'Ver Soluções' ,
		'search_items'          => 'Buscar Solução' ,
		'not_found'             => 'Não encontrado' ,
		'not_found_in_trash'    => 'Não encontrado' ,
		'featured_image'        => 'Imagem destacada' ,
		'set_featured_image'    => 'Adicionar imagem destacada' ,
		'remove_featured_image' => 'Remover imagem destacada' ,
		'use_featured_image'    => 'Usar como imagem destacada' ,
		'insert_into_item'      => 'Inserir no item' ,
		'uploaded_to_this_item' => 'Adicionado ao item' ,
		'items_list'            => 'Lista de itens' ,
		'items_list_navigation' => 'Navegação da lista de itens' ,
		'filter_items_list'     => 'Filtrar lista de itens' ,
	);
	
	$args = array(
		'label'                 => 'solucoes' ,
		'labels'                => $labels,
		'taxonomies'            => array('solution-category'),
		'supports'              =>  array( 'title' , 'thumbnail', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_in_rest'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'publicly_queryable'    => true,
		'exclude_from_search'   => true,
		'rewrite' => array('slug' => 'solucao')
	);
	register_post_type( 'solucoes', $args );

}

add_action( 'init', 'custom_post_type', 0);


function create_my_taxonomies() {
    register_taxonomy(
        'solution-category',
        'solucoes',
        array(
            'labels' => array(
                'name' => 'Categoria',
                'add_new_item' => 'Adicionar Categoria'
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
			'rewrite'           => array( 'slug' => 'solucoes' ),
        )
    );
}
add_action( 'init', 'create_my_taxonomies', 0 );


// Rewrite Rules to Solutions Taxonomies

function itera_taxonomies_generating_rule($wp_rewrite) {
    $rules = array();
    $terms = get_terms( array(
        'taxonomy' => 'solution-category',
        'hide_empty' => false,
    ));
   
    $post_type = 'solucoes';

    foreach ($terms as $term) {    
                
        $rules['solucoes/' . $term->slug . '/([^/]*)$'] = 'index.php?post_type=' . $post_type. '&resources_post_type=$matches[1]&name=$matches[1]';
                        
    }
	
	$rules['artigos/([^/]*)$'] = 'index.php?post_type=post&name=$matches[1]';

    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'itera_taxonomies_generating_rule');

function itera_solucoes_change_post_link ($permalink, $post) {
	$permalink = get_home_url() ."/artigos/" . $post->post_name . '/';
    return $permalink;
}
add_filter('post_link',"itera_solucoes_change_post_link",10,2);

function itera_solucoes_change_link( $permalink, $post ) {
    
    if( $post->post_type == 'solucoes' ) {
        $resource_terms = get_the_terms( $post, 'solution-category' );
        $term_slug = '';
        if( ! empty( $resource_terms ) ) {
            foreach ( $resource_terms as $term ) {

	            $term_slug = $term->slug;
                break;
            }
        }
        $permalink = get_home_url() ."/solucoes/" . $term_slug . '/' . $post->post_name . '/';
    }
    return $permalink;

}
add_filter('post_type_link',"itera_solucoes_change_link",10,2);