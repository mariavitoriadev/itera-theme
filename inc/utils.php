<?php


function hook_header() { 

	// Itera LP set page versions style
	if(is_page_template( 'itera-lp.php' )):

		if( have_rows('versoes_da_pagina') ):
			while( have_rows('versoes_da_pagina') ) : the_row(); 
				if(isset($_REQUEST['id_' . get_sub_field('page_version')])) {
					$class = '.page-version-' . get_sub_field('page_version') . ' .section-icons .item-icon.page-version-' . get_sub_field('page_version');
					echo '<style>' . $class . '{ display: flex }' . '</style>';
				}
			endwhile; 			
		endif;

	endif;

}
add_action('wp_head','hook_header');

function itera_theme_set_post_views($post_id) {

	$count_key = 'blog_post_view_count';
	$count = get_post_meta($post_id, $count_key, true);

	if($count == '')
		add_post_meta($post_id, $count_key, '0');
	else {
		$count = intval($count);
		$count++;
		update_post_meta($post_id, $count_key, $count);
	}

}
   
function itera_theme_track_post_views() {
   
	if ( is_single() && 'post' == get_post_type() && !is_admin() ) {
		$post_id = get_the_ID();
		itera_theme_set_post_views($post_id);
	}

}
   
add_action('wp_head', 'itera_theme_track_post_views');


function itera_theme_remove_pages_from_search() {
	global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}

add_action('init', 'itera_theme_remove_pages_from_search');

function itera_theme_truncate_excerpt($text) {
	if(!is_single() && strlen($text) > 200){
		$text = mb_substr( $text, 0, 200 );
		$text .= '...';
	}

	return $text;
}

add_filter('get_the_excerpt', 'itera_theme_truncate_excerpt');


// Header Nav Filter

function filter_header_menu($nav_menu, $args){
	if ($args->theme_location == 'header-main') {
		$nav_menu = preg_replace('/<li id="(.*?)" class="solutions-hover/','<li id="${1}" on="tap:AMP.setState({header: {menu: !header.menu}})" tabindex="0" role="button" class="solutions-hover',$nav_menu);
	}

	if ($args->theme_location == 'header-main' && is_single() && 'post' == get_post_type()):
		$nav_menu = preg_replace('/<li(.*?)class="(.*?)"(.*?)><a(.*?)>Artigos<\/a>/','<li${1}class="current-menu-item ${2}"${3}><a${4}>Artigos<\/a>',$nav_menu);
	endif;
	return $nav_menu;
}
add_filter('wp_nav_menu','filter_header_menu',10,2);

// Reading time

function reading_time() {
	$word_count = str_word_count(strip_tags(get_the_content()));
	// 1 minute by 150 words
	$readingtime = ceil($word_count / 150);
	$total = $readingtime;

	return $total;
}

// Breadcrumbs filter

function filter_wpseo_breadcrumb_links( $breadcrumbs ) { 
	if((is_single() || is_category() || is_tag()) && 'post' == get_post_type()) {
		$breadcrumbs = preg_replace('/<a href="(.*?)">Itera<\/a>/', '<a href="${1}artigos">Artigos<\/a>', $breadcrumbs);
	}
	if(is_single() && 'post' == get_post_type()) {
		$breadcrumbs = preg_replace('/<span class="breadcrumbs-separator">›<\/span>.<strong class="breadcrumb_last" aria-current="page">.*?<\/strong>/', '', $breadcrumbs);
		$breadcrumbs = preg_replace('/<span class="breadcrumbs-separator">›<\/span>.*?(<a.*<\/a>)/', '<span class="breadcrumbs-separator">›</span><strong>${1}</strong>', $breadcrumbs);

	}
	return $breadcrumbs; 
}
add_filter( 'wpseo_breadcrumb_output', 'filter_wpseo_breadcrumb_links', 10, 1 ); 

function filter_wpseo_breadcrumb($breadcrumbs) {
	if(is_single() && 'solucoes' == get_post_type()) :
		array_pop($breadcrumbs);
	endif;

	return $breadcrumbs;
}

add_filter( 'wpseo_breadcrumb_links', 'filter_wpseo_breadcrumb', 10, 1 ); 



function filter_wpseo_breadcrumb_separator( $separator ) { 
return '<span class="breadcrumbs-separator">' . $separator . '</span>'; 
};  
add_filter( 'wpseo_breadcrumb_separator', 'filter_wpseo_breadcrumb_separator', 10, 1 ); 


// LP Versions form URL to Body Class
function add_lp_versions ( $classes ) {
	
    if(is_page_template( 'itera-lp.php' )):

		if( have_rows('versoes_da_pagina') ):
			while( have_rows('versoes_da_pagina') ) : the_row(); 
				if(isset($_REQUEST['id_' . get_sub_field('page_version')])) {
					$version_set = true;
					$classes[] = 'page-version-' . get_sub_field('page_version');
				}
			endwhile; 
		endif;

		if(!isset($version_set)) {
			$classes[] = 'default-version';
		}
		
	endif;
	
    return $classes;
}
add_filter( 'body_class', 'add_lp_versions' );


remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );