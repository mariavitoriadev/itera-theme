<?php

function theme_itera_customizer_register($wp_customize) {

	$wp_customize->add_setting('logo-svg',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'logo-svg-control', array(
		'label' => 'Logo (SVG)',
		'section' => 'title_tagline',
		'settings' => 'logo-svg',
		'type' => 'textarea'
	)));

	$wp_customize->add_setting('logo-svg-white',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'logo-svg-white-control', array(
		'label' => 'Logo Branco (SVG)',
		'section' => 'title_tagline',
		'settings' => 'logo-svg-white',
		'type' => 'textarea'
	)));

	$wp_customize->add_setting('logo-png',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'logo-png-control', array(
		'label' => 'Logo (PNG)',
		'section' => 'title_tagline',
		'settings' => 'logo-png',
	)));

	// Header Menu
	$wp_customize->add_section('header-menu', array(
		'title' => 'Header Menu'
	));

	$wp_customize->add_setting('alternative-header',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'alternative-header-control', array(
		'label' => 'Ativar header alternativo?',
		'section' => 'header-menu',
		'settings' => 'alternative-header',
		'type' => 'checkbox'
	)));

	$wp_customize->add_setting('header-card-image',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'header-card-image-control', array(
		'label' => 'URL da imagem do Card 1',
		'section' => 'header-menu',
		'settings' => 'header-card-image'
	)));

	$wp_customize->add_setting('header-card-sector',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'header-card-sector-control', array(
		'label' => 'Setor do Card 1',
		'section' => 'header-menu',
		'settings' => 'header-card-sector'
	)));

	$wp_customize->add_setting('header-card-title',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'header-card-title-control', array(
		'label' => 'Título do Card 1',
		'section' => 'header-menu',
		'settings' => 'header-card-title'
	)));

	$wp_customize->add_setting('header-card-cta',array(
		'default' => ''
	));

	$wp_customize->add_control('header-card-cta-control', array(
		'label' => 'CTA do Card 1 (URL)',
		'section' => 'header-menu',
		'settings' => 'header-card-cta',
		'type' => 'url'
	));

	$wp_customize->add_setting('header-card-cta-text',array(
		'default' => ''
	));

	$wp_customize->add_control('header-card-cta-text-control', array(
		'label' => 'CTA do Card 1 (Texto)',
		'section' => 'header-menu',
		'settings' => 'header-card-cta-text',
		'type' => 'url'
	));
	
	$wp_customize->add_setting('header-card-2-image',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'header-card-2-image-control', array(
		'label' => 'URL da imagem do Card 2',
		'section' => 'header-menu',
		'settings' => 'header-card-2-image'
	)));

	$wp_customize->add_setting('header-card-2-sector',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'header-card-2-sector-control', array(
		'label' => 'Setor do Card 2',
		'section' => 'header-menu',
		'settings' => 'header-card-2-sector'
	)));

	$wp_customize->add_setting('header-card-2-title',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'header-card-2-title-control', array(
		'label' => 'Título do Card 2',
		'section' => 'header-menu',
		'settings' => 'header-card-2-title'
	)));

	$wp_customize->add_setting('header-card-2-cta',array(
		'default' => ''
	));

	$wp_customize->add_control('header-card-2-cta-control', array(
		'label' => 'CTA do Card 2 (URL)',
		'section' => 'header-menu',
		'settings' => 'header-card-2-cta',
		'type' => 'url'
	));

	$wp_customize->add_setting('header-card-2-cta-text',array(
		'default' => ''
	));

	$wp_customize->add_control('header-card-2-cta-text-control', array(
		'label' => 'CTA do Card 2 (Texto)',
		'section' => 'header-menu',
		'settings' => 'header-card-2-cta-text',
		'type' => 'url'
	));

	$wp_customize->add_setting('button-title',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'button-title-control', array(
		'label' => 'Button Title',
		'section' => 'header-menu',
		'settings' => 'button-title'
	)));
	
	
	$wp_customize->add_setting('button-url',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'button-url-control', array(
		'label' => 'Button URL',
		'section' => 'header-menu',
		'settings' => 'button-url'
	)));

	// Contact Section
	$wp_customize->add_section('contact', array(
		'title' => 'Informações de Contato'
	));

	$wp_customize->add_setting('contact-address',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'contact-adress-control', array(
		'label' => 'Endereço',
		'section' => 'contact',
		'settings' => 'contact-address'
	)));

	$wp_customize->add_setting('contact-phone',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'contact-phone-control', array(
		'label' => 'Telefone',
		'section' => 'contact',
		'settings' => 'contact-phone'
	)));

	$wp_customize->add_setting('contact-email',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'contact-email-control', array(
		'label' => 'E-mail',
		'section' => 'contact',
		'settings' => 'contact-email'
	)));

	// Integrações
	$wp_customize->add_section('integrations', array(
		'title' => 'Integrações'
	));

	$wp_customize->add_setting('api-url',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'api-url-control', array(
		'label' => 'API URL',
		'section' => 'integrations',
		'settings' => 'api-url'
	)));

	$wp_customize->add_setting('api-token',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'api-token-control', array(
		'label' => 'API Token',
		'section' => 'integrations',
		'settings' => 'api-token'
	)));

	// Social Media Section
	$wp_customize->add_section('social-networks', array(
		'title' => 'Redes Sociais'
	));

	$wp_customize->add_setting('social-networks-facebook',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'social-networks-facebook-control', array(
		'label' => 'Link Facebook',
		'section' => 'social-networks',
		'settings' => 'social-networks-facebook'
	)));

	$wp_customize->add_setting('social-networks-facebook-id',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'social-networks-facebook-id-control', array(
		'label' => 'App ID do Facebook',
		'section' => 'social-networks',
		'settings' => 'social-networks-facebook-id'
	)));

	$wp_customize->add_setting('social-networks-instagram',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'social-networks-instagram-control', array(
		'label' => 'Link Instagram',
		'section' => 'social-networks',
		'settings' => 'social-networks-instagram'
	)));

	$wp_customize->add_setting('social-networks-linkedin',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'social-networks-linkedin-control', array(
		'label' => 'Link Linkedin',
		'section' => 'social-networks',
		'settings' => 'social-networks-linkedin'
	)));

	$wp_customize->add_setting('social-networks-twitter',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'social-networks-twitter-control', array(
		'label' => 'Link Twitter',
		'section' => 'social-networks',
		'settings' => 'social-networks-twitter'
	)));

	$wp_customize->add_setting('social-networks-youtube',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'social-networks-youtube-control', array(
		'label' => 'Link Youtube',
		'section' => 'social-networks',
		'settings' => 'social-networks-youtube'
	)));

	// Tracking Section
	$wp_customize->add_section( 'tracking' , array(
		'title' => 'Tracking'
	) );

	$wp_customize->add_setting('tracking-gtm',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'tracking-gtm-control', array(
			'label'      => 'Container GTM',
			'section'    => 'tracking', 
			'settings'   => 'tracking-gtm',
	)));

	// Tracking Section
	$wp_customize->add_section( 'lp-popup' , array(
		'title' => 'LP Popup'
	) );

	$wp_customize->add_setting('lp-popup-title',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'lp-popup-title-control', array(
			'label'      => 'Título',
			'section'    => 'lp-popup', 
			'settings'   => 'lp-popup-title',
	)));

	$wp_customize->add_setting('lp-popup-cta-title',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'lp-popup-cta-title-control', array(
			'label'      => 'CTA Texto',
			'section'    => 'lp-popup', 
			'settings'   => 'lp-popup-cta-title',
	)));

	$wp_customize->add_setting('lp-popup-cta-url',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Control( $wp_customize, 'lp-popup-cta-url-control', array(
			'label'      => 'CTA Url',
			'section'    => 'lp-popup', 
			'settings'   => 'lp-popup-cta-url',
	)));

	$wp_customize->add_setting('lp-popup-image',array(
		'default' => ''
	));

	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'lp-popup-image-control', array(
		'label' => 'Imagem',
		'section' => 'lp-popup',
		'settings' => 'lp-popup-image'
	)));

}

add_action('customize_register', 'theme_itera_customizer_register');