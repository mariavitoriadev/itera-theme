<?php

function itera_theme_setup() {

    add_theme_support('post-thumbnails');
    add_theme_support('widgets');
    add_theme_support('title-tag');
    add_theme_support('amp');

}
add_action('after_setup_theme', 'itera_theme_setup');

function itera_theme_enqueue_styles() {

	$template_directory = get_template_directory_uri();
    
	wp_enqueue_script('ithera-theme-main-js', get_template_directory_uri() . '/js/main.js', '' , '2.0.0', true);

    wp_enqueue_style('swiper-css', $template_directory.'/swiper-bundle.min.css');
    wp_enqueue_style('common-css', $template_directory.'/css/common.css');
    wp_enqueue_style('header-css', $template_directory.'/css/header.css');
    wp_enqueue_style('footer-css', $template_directory.'/css/footer.css');
    

    if(is_front_page()):
        wp_enqueue_style('datas-section-css', $template_directory.'/css/home/datas-section.css');
        wp_enqueue_style('solution-card-css', $template_directory.'/css/solution-card.css');
        wp_enqueue_style('solutions-section-css', $template_directory.'/css/home/solutions-section.css');
        wp_enqueue_style('partner-section-css', $template_directory.'/css/home/partner-section.css');
        wp_enqueue_style('alice-section-css', $template_directory.'/css/home/alice-section.css');
        wp_enqueue_style('companies-section-css', $template_directory.'/css/home/companies-section.css');
        wp_enqueue_style('sidebar-css', $template_directory.'/css/sidebar.css');
        wp_enqueue_style('newsletter-css', $template_directory.'/css/newsletter.css');
        wp_enqueue_style('popular-posts-css', $template_directory.'/css/popular-posts.css');
        wp_enqueue_style('front-page-css', $template_directory.'/css/front-page.css');
    endif;

    if (!is_front_page() && is_home()):
        wp_enqueue_style('blog-home-hero-css', $template_directory.'/css/blog/home-hero.css');
        wp_enqueue_style('blog-categories-navigator-css', $template_directory.'/css/blog/categories-navigator.css');
        wp_enqueue_style('post-card-css', $template_directory.'/css/post-card.css');
        wp_enqueue_style('post-grid-css', $template_directory.'/css/post-grid.css');
        wp_enqueue_style('grid-sidebar-css', $template_directory.'/css/grid-sidebar.css');
        wp_enqueue_style('grid-pagination-css', $template_directory.'/css/grid-pagination.css');
        wp_enqueue_style('sidebar-css', $template_directory.'/css/sidebar.css');
        wp_enqueue_style('newsletter-css', $template_directory.'/css/newsletter.css');
        wp_enqueue_style('popular-posts-css', $template_directory.'/css/popular-posts.css');

    endif;

    if(is_page_template( 'page-about-us.php' )):
       wp_enqueue_style('page-about-us-css', $template_directory.'/css/page-about-us.css');
	   wp_enqueue_style('contact-section-css', $template_directory.'/css/contact-section.css');
    endif;

    if(is_page_template( 'page-alice.php' )):
       wp_enqueue_style('page-alice-css', $template_directory.'/css/page-alice.css');
    endif;

    if(is_page_template( 'page-contact.php' )):
       wp_enqueue_style('page-contact-css', $template_directory.'/css/page-contact.css');
    endif;
 
    if(is_page_template( 'submit-page.php' )):
        wp_enqueue_style('submit-page-css', $template_directory.'/css/submit-page.css');
    endif;
  
    if(is_page_template( 'itera-lp.php' )):
        wp_enqueue_style('newsletter-css', $template_directory.'/css/newsletter.css');
        wp_enqueue_style('itera-lp-css', $template_directory.'/css/itera-lp.css');
        wp_enqueue_style('companies-section-css', $template_directory.'/css/home/companies-section.css');
    endif;

    if(is_search()):
        wp_enqueue_style('sidebar-css', $template_directory.'/css/sidebar.css');
        wp_enqueue_style('newsletter-css', $template_directory.'/css/newsletter.css');
        wp_enqueue_style('popular-posts-css', $template_directory.'/css/popular-posts.css');
        wp_enqueue_style('post-grid-css', $template_directory.'/css/post-grid.css');
        wp_enqueue_style('grid-sidebar-css', $template_directory.'/css/grid-sidebar.css');
        wp_enqueue_style('grid-pagination-css', $template_directory.'/css/grid-pagination.css');
        wp_enqueue_style('post-card-css', $template_directory.'/css/post-card.css');
        wp_enqueue_style('search-css', $template_directory.'/css/search/search.css');
    endif;

    if(is_archive()):
        wp_enqueue_style('sidebar-css', $template_directory.'/css/sidebar.css');
        wp_enqueue_style('newsletter-css', $template_directory.'/css/newsletter.css');
        wp_enqueue_style('popular-posts-css', $template_directory.'/css/popular-posts.css');
        wp_enqueue_style('post-grid-css', $template_directory.'/css/post-grid.css');
        wp_enqueue_style('grid-sidebar-css', $template_directory.'/css/grid-sidebar.css');
        wp_enqueue_style('grid-pagination-css', $template_directory.'/css/grid-pagination.css'); 
        wp_enqueue_style('post-card-css', $template_directory.'/css/post-card.css');
        wp_enqueue_style('archive-hero-css', $template_directory.'/css/archive/hero.css');
    endif;

    if(is_tax('solution-category')):
        wp_enqueue_style('solution-category-css', $template_directory.'/css/solution-category.css');
    endif;


    if(is_single() && ('post' || 'solucoes') == get_post_type()):
        wp_enqueue_style('sidebar-css', $template_directory.'/css/sidebar.css');
        wp_enqueue_style('newsletter-css', $template_directory.'/css/newsletter.css');
        wp_enqueue_style('post-grid-css', $template_directory.'/css/post-grid.css');
        wp_enqueue_style('grid-sidebar-css', $template_directory.'/css/grid-sidebar.css');
        wp_enqueue_style('grid-pagination-css', $template_directory.'/css/grid-pagination.css');
        wp_enqueue_style('popular-posts-css', $template_directory.'/css/popular-posts.css');
        wp_enqueue_style('single-css', $template_directory.'/css/single.css');
        wp_enqueue_style('post-card-css', $template_directory.'/css/post-card.css');
    endif;

    if(is_404()):
        wp_enqueue_style('archive-search-hero-css', $template_directory.'/css/archive-search/hero.css');     
        wp_enqueue_style('404-page-css', $template_directory.'/css/404-page.css');     
    endif;

}

add_action('wp_enqueue_scripts', 'itera_theme_enqueue_styles');

function itera_theme_move_jquery_to_footer() {
    wp_scripts()->add_data( 'jquery', 'group', 1 );
    wp_scripts()->add_data( 'jquery-core', 'group', 1 );
    wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );
}
add_action( 'wp_enqueue_scripts', 'itera_theme_move_jquery_to_footer' );

function itera_theme_register_menus() {

	register_nav_menus( array(
		'header-main' => 'Header Main Menu',
		'header-mobile' => 'Header Menu Mobile',
        'footer-menu-itera' => 'Footer Menu Itera',
        'footer-menu-solucoes' => 'Footer Menu Soluções',
		'footer-menu-blog' => 'Footer Menu Blog'
	) );

}

add_action('init', 'itera_theme_register_menus');

function itera_theme_register_sidebar( $args ) {
    register_sidebar(
        array(
            'name' => 'Main Sidebar',
            'id' => 'main-sidebar',
            'before_widget' => '',
            'after_widget' => ''
        )
    );
};
add_action( 'widgets_init', 'itera_theme_register_sidebar' );
