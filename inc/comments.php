<?php

function blog_red_balloon_format_comment($comment, $args, $depth) {  ?>
	<li class='post-comment'>
		<div class="comment-info">               
			<p class="comment-author"><?= ucwords(strtolower(get_comment_author())) ?></p>
			<p class="comment-date"><?= get_comment_date('d.m.Y') ?></p>
		</div> 

		<div class="comment-text">
			<p><?= ucfirst(strtolower(get_comment_text())); ?></p>
		</div>
		<div class="comment-reply">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
	</li>
<?php 
} 

function blog_red_balloon_move_comment_field_to_bottom($fields) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
 
add_filter( 'comment_form_fields', 'blog_red_balloon_move_comment_field_to_bottom' );

function blog_red_balloon_comment_form_open_wrap() {
    echo '<div class="form-inputs">';
}

add_action('comment_form_before_fields', 'blog_red_balloon_comment_form_open_wrap');

function blog_red_balloon_comment_form_close_wrap() {
    echo '</div>';
}

add_action('comment_form_after_fields', 'blog_red_balloon_comment_form_close_wrap');

function blog_red_balloon_redirect_comments_false($location, $commentdata){
	wp_send_json(array(
		'message' => 'Comentário enviado com <strong>sucesso</strong> e aguardando moderação.<amp-layout class="tracking-pixel" id="comment-submit" layout="fixed" width="1" height="1"></amp-layout>'
	), 200);
}

add_filter('comment_post_redirect', 'blog_red_balloon_redirect_comments_false', 0, 2);
