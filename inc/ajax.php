<?php

function ithera_theme_filter_solutions() {

	$term_id = $_REQUEST['id'];
	$tax_string = $term_id;
	$tax_array = explode( ',', $tax_string );
	$response = array();


	$args = array( 
		'posts_per_page' => -1, 
		'post_type' => 'solucoes',
		'tax_query' => array(
			array(
			'taxonomy' => 'solution-category',
			'field' => 'term_id',
			'terms' => $tax_array
			)
		),
		'meta_query' => array(
			array(
			'key'     => 'is_solution_case',
			'value'   => true,
			'compare' => '=',
			)
		)
	);
	$solutionPosts = new WP_Query($args);  
	
	while ( $solutionPosts->have_posts() ) {
		$solutionPosts->the_post(); 
		
		$term_id = explode(',', $term_id);
		if(strlen($term_id) == 1 ) {
			$solutions_terms = get_term($term_id,'solution-category');
		}
		else {
			$solutions_terms = get_the_terms($post->ID,'solution-category');
		}
		
		$response[] = '
		<div class="solutions-posts-item">
			<a href="'. get_the_permalink() .'">'
				. get_the_post_thumbnail() .
			'<div class="solution-content"> 
				<p class="solution-post-category" href="'. get_term_link($solutions_terms[0]->term_id) .'">
					'. $solutions_terms[0]->name .'	
				</p>
				<p class="solutions-post-title">' . get_the_title() . '</p>
				<a class="read-more" href="' . get_the_permalink() . '">Saiba Mais</a>
			</div>
		</div>';
	}

	echo json_encode($response);
	wp_die();
}
add_action('wp_ajax_filter_solutions', 'ithera_theme_filter_solutions');
add_action('wp_ajax_nopriv_filter_solutions', 'ithera_theme_filter_solutions');


function ithera_theme_insert_lead() {

    $userEmail = $_POST['email'];
    $api_url = get_theme_mod('api-url');
    $api_token = get_theme_mod('api-token');
	if($_POST['name']) {
		$data = '{
		"contact": {"email": "' . $userEmail . '","firstName": "' . $_POST['name'] . '"}
		}';	
	}
	else {
		$data = '{
		"contact": {"email": "' . $userEmail . '"}
		}';
	}

	$response = send_lead('https://itera87258.api-us1.com/api/3/contacts', $data);
	
	if($response['errors']):	
		wp_send_json_error('Email já cadastrado, tente outro por favor.', 400); 
 	else :
		
		$listID = $_POST['list-id'];
		$data = '{"contactList": {"list": '. $listID . ',"contact": '. $response['contact']['id'] . ',"status": 1}}';
		$responseList = send_lead('https://itera87258.api-us1.com/api/3/contactLists', $data);

		if($_POST['company-sector']) {
			$data = '{"fieldValue": {"value":"'. $_POST['company-sector'] .'","contact":"'. $response['contact']['id'] .'","field":"17"}}';	
		
			$responseCustomFields = send_lead('https://itera87258.api-us1.com/api/3/fieldValues', $data);
		}

		if($_POST['company-function']) {
			$data = '{"fieldValue": {"value":"'. $_POST['company-function'] .'","contact":"'. $response['contact']['id'] .'","field":"18"}}';	

			$responseCustomFields = send_lead('https://itera87258.api-us1.com/api/3/fieldValues', $data);
		}

		if($_POST['company-name']) {
			$data = '{"fieldValue":  {"value":"'. $_POST['company-name'] .'","contact":"'. $response['contact']['id'] .'","field":"19"}}';	

			$responseCustomFields = send_lead('https://itera87258.api-us1.com/api/3/fieldValues', $data);
		}

    	wp_send_json_success('success', 200);

 	endif;

 	wp_die();
}


function send_lead($url, $data) {
	
	$api_url = get_theme_mod('api-url');
	$api_token = get_theme_mod('api-token');
	
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url ,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $data,
		CURLOPT_HTTPHEADER => array("api_token:" . $api_token, "Content-Type: application/json"),
	));

	$response = curl_exec($curl);
	$response = json_decode($response, true);
	curl_close($curl);

	return $response;
}

add_action('wp_ajax_insert_lead', 'ithera_theme_insert_lead');
add_action('wp_ajax_nopriv_insert_lead', 'ithera_theme_insert_lead');

function ithera_theme_get_grid_posts() {

	if(isset($_GET['page'])):
		$args['paged'] = $_GET['page'];
	else:
		echo '{"items":[]}';
		wp_die();
	endif;

	if(isset($_GET['order'])):
		$order = $_GET['order'];
		switch ($order) {
			case 'date':
				$args['orderby'] = 'date';
				$args['order'] = 'DESC';
				break;
			
			case 'views':
				$args['orderby'] = 'meta_value_num';
				$args['order'] = 'DESC';
				$args['meta_key'] = 'blog_post_view_count';
				break;

			case 'comments':
				$args['orderby'] = 'comment_count';
				$args['order'] = 'DESC';
				break;
		}
	endif;

	if(isset($_GET['tag'])) {
		$args['tag'] = $_GET['tag'];
	}

	if(isset($_GET['s'])) {
		$args['s'] = $_GET['s'];
	}

	if(isset($_GET['category'])) {
		$args['cat'] = $_GET['category'];
	}

	$args['post_status'] = 'publish';

	$grid_query = new WP_Query($args);
	$result_array = array('items' => array());

	while($grid_query->have_posts()):

		$grid_query->the_post();

		$category = get_the_category();
		$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium" );

		$result_array['items'][] = array (
			'image_url' => $image_data[0],
			'image_width' => $image_data[1],
			'image_height' => $image_data[2],
			'category_name' => $category[0]->name,
			'category_link' =>  get_term_link($category[0]->term_id),
			'post_title' => get_the_title(),
			'post_date' => get_the_date('d/m/Y'),
			'post_excerpt' => get_the_excerpt(),
			'post_link' => get_the_permalink(),
		);

	endwhile;

	if (strpos(site_url(),'localhost')):
		$url = "//localhost";
	else:
		$url = site_url();
	endif;

	wp_reset_postdata();

	echo json_encode($result_array);

	wp_die();


}

add_action('wp_ajax_get_grid_posts', 'ithera_theme_get_grid_posts');
add_action('wp_ajax_nopriv_get_grid_posts', 'ithera_theme_get_grid_posts');

