<?php

add_action( 'add_meta_boxes', 'add_lead_metaboxes' );
add_action( 'save_post', 'lead_save_box' );

function add_lead_metaboxes() {
	/* Add lead name */
	add_meta_box(
		'lead_name',
		'Nome',
		'lead_name_box',
		'lead'
	);

	/* Add lead email */
	add_meta_box(
		'lead_email',
		'Email',
		'lead_email_box',
		'lead'
	);

	/* Add lead source */
	add_meta_box(
		'lead_source',
		'Origem',
		'lead_source_box',
		'lead'
	);
}

function lead_name_box( $post ) {
	$name = get_post_meta( $post->ID, 'lead_name', true );
	echo '<input type="text" id="lead_name_input" name="lead_name" value="'. $name .'"/>';
}

function lead_email_box( $post ) {
	$email = get_post_meta( $post->ID, 'lead_email', true );
	echo '<input type="text" id="lead_email_input" name="lead_email" value="'. $email .'"/>';
}

function lead_source_box( $post ) {
	$source = get_post_meta( $post->ID, 'lead_source', true );
	echo '<input type="text" id="lead_source_input" name="lead_source" value="'. $source .'"/>';
}

function lead_save_box( $post_id ) {

	if (isset($_POST['post_type']) && 'lead' == $_POST['post_type'] ) {
		if ( current_user_can( 'edit_post', $post_id) ) {
			$lead_name = $_POST['lead_name'];
			$lead_email = $_POST['lead_email'];
			$lead_source = $_POST['lead_source'];
			update_post_meta($post_id, 'lead_name', $lead_name);
			update_post_meta($post_id, 'lead_email', $lead_email);
			update_post_meta($post_id, 'lead_source', $lead_source);
		}
		else {
			return;
		}	
	} elseif ( !current_user_can( 'edit_post', $post_id) ) {
		return;
	}

}

add_action('manage_lead_posts_columns', 'manage_leads_columns');
function manage_leads_columns($columns) {

    $date = $columns['date'];
    unset($columns['date']);
    unset($columns['title']);
    unset($columns['author']);

    $columns['lead_name'] = "Nome";
    $columns['lead_email'] = "Email";
    $columns['lead_source'] = "Origem";
    $columns['date'] = $date;

    return $columns;
}

add_action( 'manage_lead_posts_custom_column' , 'edit_leads_columns', 10, 2 );
function edit_leads_columns( $column, $post_id ) {
    switch ($column) {
        case 'lead_name' :
            $lead_name = get_post_meta($post_id,'lead_name', true);
            echo $lead_name;
            break;
        case 'lead_email' :
            $lead_email = get_post_meta($post_id,'lead_email', true);
            if (isset($lead_email))
                echo $lead_email;
            break;
        case 'lead_source' :
            $lead_source = get_post_meta($post_id,'lead_source', true);
            if (isset($lead_source))
                echo $lead_source;
            break;
    }
}