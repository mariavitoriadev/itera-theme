<?php

class Popular_Post_Widget extends WP_Widget {

    public function __construct() {     
        $widget_options = array( 
            'classname' => 'popular-post-widget',
            'description' => 'Cria uma área de posts populares',
        );
        parent::__construct( 'popular_post_widget', 'Posts Populares', $widget_options );
    }

    public function widget ($args, $instance) {    
        $title_text = $instance['title'];
        $number_of_post = $instance['number_of_post'];
        
        is_single() || is_search() || is_front_page() ? $number_of_post = $number_of_post + 1 : '';

        $args = array( 
            'posts_per_page' => $number_of_post, 
            'order' => 'DESC'
        );

        if ('solucoes' == get_post_type()) {
            $args['post_type'] = 'solucoes';
            $args['meta_query'] = array(
                array(
                    'key'     => 'is_solution_case',
                    'value'   => true,
                    'compare' => '!=',
                )
            );
        }

        $popularpost = new WP_Query($args);

        ?>
        <div class="desktop widget-popular-posts-wrapper">
            <p class='widget-popular-posts-title'><?=$title_text?></p>
            <div class='widget-popular-posts'>
             <?php  while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
                <a href="<?php the_permalink()?>" class="widget-popular-posts-item">
					<?php  if (function_exists('is_amp_endpoint') && is_amp_endpoint()) :
						echo get_the_post_thumbnail( $popularpost->ID , 'full', array('layout' => 'fill', 'srcset' => wp_get_attachment_image_srcset(get_post_thumbnail_id())));
					else: 
						echo get_the_post_thumbnail( $popularpost->ID , 'medium', array('srcset' => ''));
					endif; ?>
					<div>
						<p class="widget-popular-post-title"><?= get_the_title(); ?></p>
						<span class="read-more" href="<?php the_permalink() ?>">Saiba Mais</span>
					</div>
                </a>
            <?php 
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
        </div>
        <?php 
    }

    public function form( $instance ) {
       
        $title = $this->get_field_name('title');
        $number_of_post = $this->get_field_name('number_of_post');

        ?>
        <p>
            <label for="<?= $title ?>">Título:</label>
            <input class="widefat" id="<?= $title ?>" name="<?= $title ?>" type="text" value="<?= $instance['title'] ?>" />
        </p>
        <p>
            <label for="<?= $number_of_post ?>">Quantidade de Posts:</label>
            <input class="widefat" id="<?= $number_of_post ?>" name="<?= $number_of_post ?>" type="text" value="<?= $instance['number_of_post'] ?>" />
        </p>
     
        <?php
    }
}

function itera_theme_register_popular_post_widget() {

    register_widget('Popular_Post_Widget');

}

add_action('widgets_init', 'itera_theme_register_popular_post_widget');