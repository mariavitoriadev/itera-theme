<?php

class Sidebar_Widget extends WP_Widget {

    public function __construct(){
        $widget_options = array(
            'classname' => 'sidebar-widget',
            'description' => 'Create a Sidebar Widget'
        );
        parent::__construct('sidebar_widget', 'Sidebar', $widget_options);
    }


    public function widget($args, $instance) {
        $main_text = $instance['main_text'];
        $cta_text = $instance['cta_text'];
        ?>

        <div class="sidebar-widget">
            <div class="about-me">
                <p class="title">Quem sou eu</p>
                <amp-img class="about-me-img" src="https://mochilondon.com/wp-content/uploads/2020/04/IMG_4213-e1587662474876.jpg" layout="responsive" width="200" height="200"></amp-img>
                <p class="text">Sou a London, paulista de 22 anos que arrumou o mochilão e decidiu viajar o mundo. Minha primeira aventura é pela América Latina, e aqui é um espacinho que separei para contar minhas aventuras e dar dicas para pessoas que também desejam escolher o caminho da liberdade :)</p>
            </div>
            <div class="youtube">
                <p class="title">Vlogs no Youtube</p>
                <amp-youtube
                  data-videoid="_gacMakMVCo"
                  layout="responsive"
                  width="480"
                  height="270"
                ></amp-youtube>
            </div>
            <div class="follow-me">
                <p class="title">Siga me</p>
                <div class="social-networks">
                    <a class="social-network-icon" href="<?= get_theme_mod('social-networks-facebook') ?>" target="_blank">
                        <?= file_get_contents( get_template_directory().'/images/facebook.svg') ?>
                    </a>
                    <a class="social-network-icon" href="<?= get_theme_mod('social-networks-instagram') ?>" target="_blank">
                        <?= file_get_contents( get_template_directory().'/images/instagram.svg') ?>
                    </a>
                    <a class="social-network-icon" href="<?= get_theme_mod('social-networks-youtube') ?>" target="_blank">
                        <?= file_get_contents( get_template_directory().'/images/youtube.svg') ?>
                    </a>
                </div>
            </div>
        </div>
    
        <?php echo $args['after_widget'];
    }

    public function form ($instance) {

        $main_text = $this->get_field_name('main_text');
        $cta_text = $this->get_field_name('cta_text');

        ?>
        <p>
            <label for="<?= $main_text ?>">Texto principal: </label>
            <input class="widefat" id="<?= $main_text ?>" name="<?= $main_text ?>" type="text" value="<?= $instance['main_text'] ?>" />
        </p>
        <p>
            <label for="<?= $cta_text ?>">Texto do botão:</label>
            <input class="widefat" id="<?= $cta_text ?>" name="<?= $cta_text ?>" type="text" value="<?= $instance['cta_text'] ?>" />
        </p>

        <?php
    }
}

function blog_mochilondon_register_sidebar_widget() {

    register_widget('Sidebar_Widget');

}

add_action('widgets_init', 'blog_mochilondon_register_sidebar_widget');