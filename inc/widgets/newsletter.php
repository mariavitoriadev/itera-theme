<?php

class Newsletter_Widget extends WP_Widget {

    public function __construct(){
        $widget_options = array(
            'classname' => 'newsletter-widget',
            'description' => 'Create a Newsletter Widget'
        );
        parent::__construct('newsletter_widget', 'Newsletter', $widget_options);
    }


    public function widget($args, $instance) {
        $main_text = $instance['main_text'];
        $second_text = $instance['second_text'];
        $cta_text = $instance['cta_text'];
        ?>

        <div class="newsletter-widget">
            <div class="newsletter-widget-content">
                <p class="newsletter-widget-main-text"><?= $main_text ?></p>
                <div class="single-post-wrapper">
                    <p class="newsletter-widget-second-text" id="newsletter-widget-second-text"><?= $second_text ?></p>
                    <form
                        id="newsletter-widget-form"
                        class="newsletter-widget-form"
                        method="POST"
                        target="_top"
                        <?php  if (function_exists('is_amp_endpoint') && is_amp_endpoint()) : ?>
                        action-xhr="<?php bloginfo('url'); ?>/wordpress/wp-admin/admin-ajax.php?action=insert_lead"
                        custom-validation-reporting="show-all-on-submit"
    					on="submit-success:newsletter-form-wrapper.hide,newsletter-widget-second-text.hide"
                        <?php else: ?>
                            action="<?php bloginfo('url'); ?>/wordpress/wp-admin/admin-ajax.php?action=insert_lead"
                        <?php endif;?>  
                    >   
                        <div class="newsletter-form-wrapper" id="newsletter-form-wrapper">
                            <label class="newsletter-widget-label">
                                <input name="email" type="email" placeholder="Seu e-mail*" id="newsletter-widget-email" class="newsletter-widget-email">
                                <span class="contact-form-alert-msg hide" visible-when-invalid="valueMissing" validation-for="newsletter-widget-email">* Campo Obrigatório</span>
                                <span class="newsletter-widget-alert-msg hide" visible-when-invalid="typeMismatch" validation-for="newsletter-widget-email">* E-mail Inválido</span>
                            </label>

                            <input type="hidden" name="nonce" value="<?= wp_create_nonce('insert_lead') ?>">
                            <input type="hidden" class="list-id" name="list-id" value="2">
                            <input class="newsletter-widget-submit" type="submit" value="<?= $cta_text ?>">
                        </div>

                        <?php 
                        if (function_exists('is_amp_endpoint') && is_amp_endpoint()) : ?>
                       
                            <div class="newsletter-widget-message success" submit-success>
                                <amp-layout class="tracking-pixel" id="newsletter-success-footer" layout="fixed" width="1" height="1"></amp-layout>
								<p>E-mail cadastrado com sucesso, em breve você receberá as últimas novidades sobre <a href="/inteligencia-artificial/" target"_blank">inteligência artificial</a> e <a href="/transformacao-digital/" target"_blank">tecnologia.</a></p>
                            </div>

                            <div class="newsletter-widget-message error" submit-error>
                                 <template type="amp-mustache">
                                    <p>{{data}}</p>
                                </template>
                                <amp-layout class="tracking-pixel" id="newsletter-error" layout="fixed" width="1" height="1"></amp-layout>
                            </div>     
                        <?php endif; ?>                   
                    </form>
                </div>
            </div>
        </div>
    
        <?php echo $args['after_widget'];
    }

    public function form ($instance) {

        $main_text = $this->get_field_name('main_text');
        $second_text = $this->get_field_name('second_text');
        $cta_text = $this->get_field_name('cta_text');

        ?>
        <p>
            <label for="<?= $main_text ?>">Texto principal: </label>
            <input class="widefat" id="<?= $main_text ?>" name="<?= $main_text ?>" type="text" value="<?= $instance['main_text'] ?>" />
        </p>
        <p>
            <label for="<?= $second_text ?>">Texto Secundário: </label>
            <input class="widefat" id="<?= $second_text ?>" name="<?= $second_text ?>" type="text" value="<?= $instance['second_text'] ?>" />
        </p>
        <p>
            <label for="<?= $cta_text ?>">Texto do botão:</label>
            <input class="widefat" id="<?= $cta_text ?>" name="<?= $cta_text ?>" type="text" value="<?= $instance['cta_text'] ?>" />
        </p>

        <?php
    }
}

function itera_theme_register_newsletter_widget() {

    register_widget('Newsletter_Widget');

}

add_action('widgets_init', 'itera_theme_register_newsletter_widget');