<?php /* Template Name: Submit Page */ ?>
<?php get_header('amp'); ?>
<main class="page-submit">
	<div class="minor-container">
		<span class="page-icon"></span>
		<p class="page-subtitle">Tudo certo!</p>
		<h2 class="page-title">Você está cada vez mais perto de <strong>aumentar sua eficiência com Inteligência Artificial.</strong></h2>
        <p class="page-description">Nossa equipe de especialistas já recebeu sua solicitação e entrará em contato em breve.</p>
        <form id="search-form" action="<?= home_url(); ?>" class="search-form">
			<input type="text" class="input-text" name="s" placeholder="digite para fazer uma busca...">
			<input type="submit" class="search-button" role="button" tabindex="0" value="">
			<?= file_get_contents( get_template_directory() . '/images/search.svg'); ?>
		</form>
		<span class="page-layers-background desktop">
			<span class="layer-1"></span>
			<span class="layer-2"></span>
			<span class="layer-3"></span>
			<span class="layer-4"></span>
		</span>
	</div>
</main>
<?php get_footer('amp'); ?>