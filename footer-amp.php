	<footer>
		<div class="footer-content minor-container">
			<div class="social-networks-wrapper">
				<div class="social-networks">
					<a class="social-network-icon" href="<?= get_theme_mod('social-networks-linkedin') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/linkedin.svg') ?>
					</a>
					<a class="social-network-icon" href="<?= get_theme_mod('social-networks-facebook') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/facebook.svg') ?>
					</a>
					<a class="social-network-icon" href="<?= get_theme_mod('social-networks-instagram') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/instagram.svg') ?>
					</a>
				</div>
			</div>
			<div class="footer-menus">
				<div class="footer-menu-item menu-itera">
					<p>Itera</p>
					<?php wp_nav_menu(array('theme_location' => 'footer-menu-itera')) ?>
				</div>
				<div class="footer-menu-item menu-solucoes">
					<p>Soluções</p>
					<?php wp_nav_menu(array('theme_location' => 'footer-menu-solucoes')) ?>
				</div>
				<div class="footer-menu-item menu-blog">
					<p>Artigos</p>
					<?php wp_nav_menu(array('theme_location' => 'footer-menu-blog')) ?>
				</div>
			</div>
		</div>
		<div class="footer-bottom container">
			<div class="footer-copyright">
				<div class="logo">
					<a href="<?= get_home_url()  ?>">
						<?= get_theme_mod('logo-svg-white') ?>
					</a>
				</div>
				<p class="copyright">© <?= date("Y") ?> Itera Inovacao e Desenvolvimento 
				<br>Tecnologico Ltd a ME <br>
				CNPJ: 09.324.695/0001-31</p>
			</div>
			<div class="footer-addresses">
				<span>
					<?= file_get_contents( get_template_directory().'/images/location.svg') ?>
				</span>
				<div class="footer-addresses-wrapper">
					<p>
						<span>Onovolab</span><br>
						<strong>Rua Aquidaban, 1 - Sala 11</strong><br>
						Centro. 13560-120<br>
						São Carlos - SP
					</p>
					<p>
						<span>Inovabra</span><br>
						<strong>Av. Angélica, 2529, 8° Andar</strong><br>
						Bela Vista. 01227-200<br>
						São Paulo - SP
					</p>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	</body>
</html>