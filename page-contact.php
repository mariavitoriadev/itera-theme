<?php /* Template Name: Contato */ ?>

<?php get_header(); ?>
<div class="common-bar minor-container">
	<div class="breadcrumbs">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
	</div>
</div>

<main class="page-contact">
	<section class="form-section minor-container ">
		<div class="contact-header">
			<p class="section-title">Entre em contato com a gente e <strong>comece sua transformação.</strong></p>
			<p class="section-description">Acreditamos em soluções simples e eficientes para seus desafios de negócios.</p>
		</div>
		<form class="contact-form">
			<div class="line-wrapper">
				<input type="text" placeholder="Seu nome *">
				<input type="email" placeholder="Seu e-mail corporativo *">
			</div>
			<div class="line-wrapper">
				<input type="text" placeholder="Seu telefone">
				<input type="text" placeholder="Setor de atuação">
			</div>
			<textarea placeholder="Sua mensagem *" ></textarea>
			<div class="line-wrapper">
				<div class="input-checkbox">
					<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
					<span></span>
					<label for="vehicle1">Quero receber novidades ITERA para meu setor.</label>
				</div>
				<input type="submit" class="submit" value="Enviar mensagem">
			</div>
		</form>
	</section>
	<div class="container layers-section">
		<div class="contact-svg">
			<?= file_get_contents( get_template_directory().'/images/page-contact.svg') ?>
		</div>
		<div class="covid-warning">
			<p class="tab">COVID-19</p>
			<p class="text">Nossa equipe está trabalhando de maneira remota. Entre em contato pelo formulário ou envie um email para contato@itera.com.br</p>
		</div>
	</div>
	<div class="contact-map">
		<div class="minor-container">
			<p class="section-title">Acreditamos no potencial de <strong>ecossistemas de inovação.</strong></p>
			<p class="section-description">Acreditamos em soluções simples e eficientes para seus desafios de negócios.</p>
		</div>
		<div class="maps-wrapper">
			<div class="map map-1">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3698.668947104076!2d-47.896620849048!3d-22.023992185396455!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce7cc8035a450f%3A0xbc6b07cd920f459e!2sItera%20-%20Inova%C3%A7%C3%A3o%20e%20Desenvolvimento%20Tecnol%C3%B3gico!5e0!3m2!1spt-BR!2sbr!4v1596752136797!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				<div class="card-map">
					<img src="<?= get_field('image-card-1')['sizes']['medium'] ?>">
					<div class="card-map-content">
						<p class="title">Onovolab</p>
						<p class="address">Rua Aquidaban, 1 - Sala 11<br>Centro. 13560-120<br>São Carlos - SP</p>
					</div>
				</div>
			</div>
		
			<div class="map map-2">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.3975393937244!2d-46.66442194902489!3d-23.554161684611305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce582dc6b93b57%3A0x410700cc6a1d872f!2sAv.%20Ang%C3%A9lica%2C%202529%20-%20Bela%20Vista%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2001227-200!5e0!3m2!1spt-BR!2sbr!4v1596826440842!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" 	allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				<div class="card-map">
					<img src="<?= get_field('image-card-2')['sizes']['medium'] ?>">
					<div class="card-map-content">
						<p class="title">Habitat Inovabra</p>
						<p class="address">Av. Angélica, 2529, 8º Andar<br>Bela Vista. 01227-200<br>Sao Paulo - SP</p>
					</div>
				</div>
			</div>
	</main>
</main>
<?php get_footer(); ?>
