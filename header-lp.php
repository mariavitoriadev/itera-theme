<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W74JPNM');</script>
	<!-- End Google Tag Manager -->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W74JPNM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<header class="header">
		<div class="header-wrapper container">
			<div class="logo">
				<div class="logo">
					<?= is_front_page() ? '<h1>' : '' ?>
					<a href="<?= get_home_url()  ?>">
						<?= get_theme_mod('logo-svg-white') ?>
					</a>
					<?= is_front_page() ? '</h1>' : '' ?>
				</div>
			</div>
			<div class="menu-main-wrapper desktop">
				<a class="header-cta desktop open-popup"><?= get_field('header_cta') ['title']?></a>
			</div>
		</div>
		<div class="header-bar hidden">
            <div class="container header-bar-wrapper">
                <div class="header-links mobile">
                    <select>
                        <?php
                        if( have_rows('header_links') ):
                            while( have_rows('header_links') ) : 
                            the_row(); ?>
                            <option value="<?= get_sub_field('header_link')['url'] ?>"><?= get_sub_field('header_link')['title'] ?></option>
                            <?php endwhile; 
                        endif; ?>
                    </select>				
                </div>
                <div class="header-links desktop">
                    <?php
                    if( have_rows('header_links') ):
                        while( have_rows('header_links') ) : 
                        the_row(); ?>
                        <a href="<?= get_sub_field('header_link')['url'] ?>"><?= get_sub_field('header_link')['title'] ?></a>
                        <?php endwhile; 
                    endif; ?>
                </div>
                <div class="menu-main-wrapper">
                    <a class="header-cta open-popup"><?= get_field('header_cta') ['title']?></a>
                </div>
            </div>
        </div>
	</header>