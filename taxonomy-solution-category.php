<?php get_header(); ?>

<div class="common-bar minor-container">
	<div class="breadcrumbs">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
	</div>
</div>

<?php
$term = get_queried_object();
$title = get_field('page-title', $term);
$description = term_description($term);
$image = get_field('category_image',  $term)['url']; 
?>

<main class="solutions-category">
	<section class="solutions-category-hero">
		<div class="solutions-category-content minor-container">
			<h1 class="title"><?= $title ?></h1>
			<div class="description"><?= $description ?></div>
			<?php if(get_field('whitepaper-file', $term)): ?>
				<script type="text/javascript">
					var whitepaperUrl = '<?= get_field('whitepaper-file', $term) ?>';
				</script>
				<a href="#" class="whitepaper cta-button"><?= get_field('page-cta', $term) ?></a>
			<?php endif ?>
		</div>
	</section>
	<div class="white-space-wrapper container"></div>
	<section class="section-1">
		<div class="section-1-image-wrapper container"> 
			<img class="section-1-image" src="<?= $image ?>">
		</div>
		<div class="minor-container section-1-header">
			<h2 class="section-title"><?= get_field('section-1-title', $term) ?></h2>
			<div class="section-description"><?= get_field('section-1-description', $term) ?>
			</div>
		</div>
	</section>
	<section class="section-2">
		<div class="section-layer">
			<?= file_get_contents( get_template_directory().'/images/solution-category-section-2.svg') ?>
			<img class="section-1-image" src="<?= get_field('section-2-icon',  $term)['sizes']['thumbnail'] ?>">
		</div>
		<div class="minor-container section-2-header">
			<h2 class="section-title"><?= get_field('section-2-title', $term) ?></h2>
			<div class="section-description"><?= get_field('section-2-description', $term) ?>
			</div>
		</div>
	</section>
	<section class="container carousel-section">
		<div class="carousel-section-container">
			<div class="swiper-wrapper">
			<?php
				if( have_rows('category-cards', $term) ):
					while( have_rows('category-cards', $term) ) : 
					the_row(); ?>
						<div class="swiper-slide">
							<div class="slide-icon">
								<img src="<?= get_sub_field('card-icon')['url'] ?>">
							</div>
							<div class="swiper-slide-content">
								<h3 class="title"><?= get_sub_field('card-title') ?></h3>
								<div class="description">
									<?= get_sub_field('card-description') ?>
								</div>
							</div>
						</div>
					<?php endwhile; 
				endif; ?>					
			</div>
			<!-- Add Pagination -->
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
		<a class="section-cta" href="<?= get_field('carousel-cta', $term)['url'] ?>" ><?= get_field('carousel-cta', $term)['title'] ?></a>
	</section>
	<section class="section-solutions">
		<div class="minor-container section-solutions-header">
			<h2 class="section-title"><?= get_field('solutions-title', $term) ?></h2>
			<div class="section-description"><?= get_field('solutions-description', $term) ?>
			</div>
		</div>
		<div class='solutions-posts container'>
         <?php
			$solutionPosts = new WP_Query( array( 
	            'posts_per_page' => -1, 
	            'post_type' => 'solucoes',
	            'tax_query'         => array(
			        array(
			            'taxonomy'  => 'solution-category',
			            'terms'     => get_queried_object()->term_id
			        )
			    )
	            
	        ) );  
         	while ( $solutionPosts->have_posts() ) : 
         	$solutionPosts->the_post(); ?>
            <div class="solutions-posts-item">
                <a href="<?php the_permalink()?>">
                    <?= get_the_post_thumbnail( $solutionPosts->ID , 'full', array('layout' => 'fill', 'srcset' => wp_get_attachment_image_srcset(get_post_thumbnail_id()))); ?>
                    <div class="solution-content">
                        <p class="solutions-post-title"><?= get_the_title(); ?></p>
                        <a class="read-more" href="<?php the_permalink() ?>">Saiba Mais</a>
                    </div>
                </a>
            </div>
        <?php 
        endwhile;
        wp_reset_postdata();
        wp_reset_query();
        ?>
        </div>
	</section>
	<?php if(get_field('whitepaper-file', $term)): ?>
		<div class="download-popup container hidden" id="download-popup">
			<img src="<?= $image ?>" >
			<div class="popup-content">
				<p class="tab">Saiba mais</p>
				<p class="title"><?= $title ?></p>
				<form
				id="popup-form"
				class="download-popup-form newsletter-widget"
				method="POST"
				target="_top"
				action="<?php bloginfo('url'); ?>/wordpress/wp-admin/admin-ajax.php?action=insert_lead"
				>   
				<div class="newsletter-form-wrapper">
					<label class="newsletter-widget-label">
						<input name="name" type="text" placeholder="Seu nome*" id="popup-name" class="newsletter-widget-email">
						<span class="contact-form-alert-msg name hide">* Campo Obrigatório</span>
					</label>
					<label class="newsletter-widget-label">
						<input name="email" type="email" placeholder="Seu e-mail*" id="popup-email" class="newsletter-widget-email">
						<span class="contact-form-alert-msg email hide">* Campo Obrigatório</span>
						<span class="newsletter-widget-alert-msg email hide">* E-mail Inválido</span>
					</label>
					<div class="input-checkbox">
						<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
						<span></span>
						<label for="vehicle1">Quero receber novidades ITERA para meu setor.</label>
					</div>

					<input type="hidden" name="nonce" value="<?= wp_create_nonce('insert_lead') ?>">
					<input type="hidden" class="list-id" name="list-id" value="3">
					<input class="newsletter-widget-submit" type="submit" value="Obtenha acesso">
				</div>              
				</form>
			</div>
			<span class="close-popup">
				<?= file_get_contents( get_template_directory().'/images/menu-close.svg') ?>
			</span>
		</div>
		<div class="popup-overlay hidden"></div>
	<?php endif; ?>
</main>

<?php get_footer(); ?>
