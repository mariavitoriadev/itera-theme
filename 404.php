<?php get_header('amp'); ?>
<main class="page-404">
	<div class="minor-container">
		<span class="page-icon"></span>
		<p class="page-subtitle">Ops...</p>
		<h2 class="page-title">A página que você está procurando não existe.</h2>
		<form id="search-form" action="<?= home_url(); ?>" class="search-form">
			<input type="text" class="input-text" name="s" placeholder="digite para fazer uma busca...">
			<input type="submit" class="search-button" role="button" tabindex="0" value="">
			<?= file_get_contents( get_template_directory() . '/images/search.svg'); ?>
		</form>
		<span class="page-number-background">404</span>
		<span class="page-layers-background desktop">
			<span class="layer-1"></span>
			<span class="layer-2"></span>
			<span class="layer-3"></span>
			<span class="layer-4"></span>
		</span>
	</div>
</main>
<?php get_footer('amp'); ?>