<?php get_header('amp'); ?>
<?php get_template_part('template-parts/archive/hero'); ?>
<div class="grid-sidebar container">
	<?php get_template_part('template-parts/common/post-grid'); ?>
	<div class="sidebar-widget">
		<?php dynamic_sidebar( 'main-sidebar' ); ?>
	</div>
</div>
<?php get_footer('amp'); ?>