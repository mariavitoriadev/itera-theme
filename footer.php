	<footer>
		<div class="footer-content minor-container">
			<div class="social-networks-wrapper">
				<div class="social-networks">
					<a class="social-network-icon" href="<?= get_theme_mod('social-networks-linkedin') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/linkedin.svg') ?>
					</a>
					<a class="social-network-icon" href="<?= get_theme_mod('social-networks-facebook') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/facebook.svg') ?>
					</a>
					<a class="social-network-icon" href="<?= get_theme_mod('social-networks-instagram') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/instagram.svg') ?>
					</a>
				</div>
			</div>
			<div class="footer-menus">
				<div class="footer-menu-item menu-itera">
					<p>Itera</p>
					<?php wp_nav_menu(array('theme_location' => 'footer-menu-itera')) ?>
				</div>
				<div class="footer-menu-item menu-solucoes">
					<p>Soluções</p>
					<?php wp_nav_menu(array('theme_location' => 'footer-menu-solucoes')) ?>
				</div>
				<div class="footer-menu-item menu-blog">
					<p>Artigos</p>
					<?php wp_nav_menu(array('theme_location' => 'footer-menu-blog')) ?>
				</div>
			</div>
		</div>
		<div class="footer-bottom container">
			<div class="footer-copyright">
				<div class="logo">
					<a href="<?= get_home_url()  ?>">
						<?= get_theme_mod('logo-svg-white') ?>
					</a>
				</div>
				<p class="copyright">© <?= date("Y") ?> Itera Inovacao e Desenvolvimento 
				<br>Tecnologico Ltd a ME <br>
				CNPJ: 09.324.695/0001-31</p>
			</div>
			<div class="footer-addresses">
				<span>
					<?= file_get_contents( get_template_directory().'/images/location.svg') ?>
				</span>
				<div class="footer-addresses-wrapper">
					<p>
						<span>Onovolab</span><br>
						<strong>Rua Aquidaban, 1 - Sala 11</strong><br>
						Centro. 13560-120<br>
						São Carlos - SP
					</p>
					<p>
						<span>Inovabra</span><br>
						<strong>Av. Angélica, 2529, 8° Andar</strong><br>
						Bela Vista. 01227-200<br>
						São Paulo - SP
					</p>
				</div>
			</div>
		</div>
	</footer>
	<div class="lp-popup hidden" id="lp-popup">
		<a href="<?= get_theme_mod('lp-popup-cta-url') ?>">
			<img class="image" src="<?= get_theme_mod('lp-popup-image') ?>">
			<p class="title"><?= get_theme_mod('lp-popup-title') ?></p>
			<p class="cta"><?= get_theme_mod('lp-popup-cta-title') ?></p>
		</a>
		<span class="close-popup" id="lp-close-popup">
			<?= file_get_contents( get_template_directory().'/images/close-white.svg') ?>
		</span>
	</div>
	<div class="lp-popup-overlay hidden" id="lp-popup-overlay" ></div>
	<?php wp_footer(); ?>
	<script type="text/javascript">
		(function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
		vgo('setAccount', '67023912');
		vgo('setTrackByDefault', true);

		vgo('process');
	</script>
	</body>
</html>