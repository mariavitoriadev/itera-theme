<?php /* Template Name: Sobre Nós */ ?>
<?php get_header(); ?>

<div class="common-bar minor-container">
	<div class="breadcrumbs">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
	</div>
</div>
<main class="about-us">
	<div class="page-hero">
		<div class="minor-container">
			<h1 class="hero-title">
				Transformação, inovação e tecnologia estão em nosso DNA.
			</h1>
			<p class="hero-description">A ITERA é a ponte entre grande volumes dados não estruturados em texto e resultados eficientes de negócio. Acreditamos no potencial da transformação digital e atuamos criando soluções tangíveis e personalizadas para serem integradas no cotidiano operacional. </p>
		</div>
	</div>
	
	<?php get_template_part('template-parts/about-us/timeline'); ?>
	
	<div class="inovation-section container">
		<div class="inovation-icons">
			<?= file_get_contents( get_template_directory().'/images/about-us-inovation.svg') ?>
		</div>


		<div class="minor-container inovation-header">
			<p class="section-tab">Inovação</p>
			<h2 class="section-title"><strong>Pesquisa e desenvolvimento</strong> são nossas bases para excelência técnica e inovação.</h2>
			<p class="section-description">Nascemos no ecossistema empreendedor e inovador de São Carlos/SP, em parcerias com as principais instituições fomentadoras de pesquisa e universidades do Brasil. Contamos com um time apaixonado por dados e desafios, feito por equipes autogerenciáveis, multidisciplinares e colaborativas, dedicadas à execução de entregas contínuas e busca constante da excelência.</p>
			<a class="cta cta-blog" href="/artigos/" >Acesse artigos publicados em nosso blog</a>
			<a class="cta cta-contact">Trabalhe conosco</a>
		</div>
	</div>

	<div class="technology-section">

		<div class="minor-container technology-header">
			<p class="section-tab">Tecnologia</p>
			<h2 class="section-title">Acreditamos no potencial transformador da <strong>inteligência artificial.</strong></h2>
			<p class="section-description">Sabemos como a tecnologia está revolucionando negócios. A Inteligência Artificial permite que as empresas conheçam e sirvam melhor seus clientes, maximizem resultados e ampliem horizontes.</p>
		</div>

		<div class="container technology-container-wrapper">
			<div class="technology-container">
					<div class="swiper-wrapper">
						<?php
						if( have_rows('carousel_itens') ):
							while( have_rows('carousel_itens') ) : 
							the_row(); ?>
								<div class="swiper-slide">
									<div class="slide-icon">
										<img src="<?= get_sub_field('carousel_icon')['url'] ?>">
									</div>
									<div class="swiper-slide-content">
										<p class="title"><?= get_sub_field('carousel_title') ?></p>
										<div class="description">
											<?= get_sub_field('carousel_description') ?>
										</div>
									</div>
								</div>
							<?php endwhile; 
						endif; ?>					
					</div>
					<!-- Add Pagination -->
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
			</div>
		</div>

	</div>
	<?php get_template_part('template-parts/contact-section'); ?>
</main>
<?php get_footer(); ?>