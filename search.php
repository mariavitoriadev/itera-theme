<?php 
	get_header('amp');

	global $wp_query;
	$total_results = $wp_query->found_posts;
	
	get_template_part('template-parts/search/hero'); 

	if($total_results > 0): ?>
		<div class="grid-sidebar container">
			<?php get_template_part('template-parts/common/post-grid'); ?>
			<div class="sidebar-widget">
				<?php dynamic_sidebar( 'main-sidebar' ); ?>
			</div>
		</div>
	<?php else:
?>
<div class="not-found-posts minor-container">
	<form id="search-form" action="<?= home_url(); ?>" class="search-form">
		<input type="text" class="input-text" name="s" placeholder="Clique aqui para fazer uma nova busca...">
		<input type="submit" class="search-button" role="button" tabindex="0" value="">
		<?= file_get_contents( get_template_directory() . '/images/search.svg'); ?>
	</form>
</div>
<div class="sidebar-widget container">
	<?php dynamic_sidebar( 'main-sidebar' ); ?>
</div>
<?php 
	endif;
	get_footer('amp'); 
?>