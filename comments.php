<section class="post-comments container" id="respond">
    <div class="comments-wrapper">
        <h3 class="comment-title container">Deixe seu comentário:</h3>
          <?php comment_form(array(
            'class_form' => 'comment-form',
            'comment_notes_before' => '',
            'title_reply' => '',
            'title_reply_to' => '',
            'label_submit' => 'Enviar',
            'fields' => array(
                'author' => '<input class="form-author" placeholder="Nome" name="author" type="text" required="required ">',
                'email'  => '<input class="form-email" placeholder="E-mail" name="email" type="email" required="required">',
                'cookies' => ''
            ),
            'comment_field' => '<textarea placeholder="Mensagem" name="comment" rows="6" required="required"></textarea>',
            'format' => 'html5'
            )); ?>
    </div>
    <?php if ( have_comments() ) : ?>
        <div class="comment-list container">
            <?php $number_comments = get_comments_number(); ?>
            <p class="comments-number"><?= $number_comments > 1 ? $number_comments . ' comentários' : $number_comments . ' comentário' ?></p>
          <?php wp_list_comments('type=comment&callback=blog_red_balloon_format_comment&max_depth=2'); ?>
        </div>
  <?php endif; ?>
</section>
