<?php

require 'inc/setup.php';
require 'inc/utils.php';
require 'inc/ajax.php';
require 'inc/customizer.php';
require 'inc/custom-post-types.php';

// Widgets
require 'inc/widgets/newsletter.php';
require 'inc/widgets/popular-posts.php';
