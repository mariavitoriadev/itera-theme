<?php get_header('amp'); ?>
	<?php get_template_part('template-parts/blog/home-hero'); ?>
	<?php get_template_part('template-parts/blog/categories-navigator'); ?>
	<div class="grid-sidebar container">
		<?php get_template_part('template-parts/common/post-grid'); ?>
		<div class="sidebar-widget">
			<?php dynamic_sidebar( 'main-sidebar' ); ?>
		</div>
	</div>

<?php get_footer('amp'); ?>