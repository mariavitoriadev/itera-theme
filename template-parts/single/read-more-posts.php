<?php 
	$args = array(
		'post__not_in' => array(get_the_ID()),
		'ignore_sticky_posts' => 1
	);
	$query = new WP_Query($args);
?>
<div class="read-more-posts">
	<p class="read-more-title container">Posts similares :</p>
	<amp-carousel
		class="read-more-carousel"
		layout="fixed-height"
		height="296px"
		type="carousel"
		controls
	>
	<?php while($query->have_posts()): $query->the_post(); ?>
		<div class="read-more-carousel-item">
			<?php get_template_part('template-parts/common/post-card'); ?>
		</div> 
	<?php endwhile; wp_reset_postdata(); ?>
	</amp-carousel>
</div>
