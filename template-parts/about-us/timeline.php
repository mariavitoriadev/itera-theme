<div class="timeline-section-wrapper container">
</div>

<div class="timeline-section">

	<div class="timeline-thumbnail">
		<?= get_the_post_thumbnail() ?>
	</div>

	<div class="minor-container timeline-header">
		<p class="section-tab">Transformação</p>
		<p class="section-title">Nos orgulhamos de <strong>nossa história.</strong></p>
		<p class="section-description">Nossa trajetória desde nossa fundação, em 2008, mostra como acreditamos no impacto da transformação através da tecnologia e como ganhamos espaço no cenário da inovação nacional.</p>
	</div>

	<div class="timeline">
	
		<div class="timeline-tabs-container container">
		<?php 
			$tabIndex = 0;
			if( have_rows('timeline') ):
			while( have_rows('timeline') ) : 
				the_row(); ?>
					<div class="tab <?= $tabIndex++ < 1 ? 'selected' : '' ?>">
						<p class="tab-year"><?= get_sub_field('timeline_year') ?></p>
					</div>
			<?php endwhile; 
		endif; ?>
		</div>

		<div class="timeline-carousel-progressbar">
			<span></span>
		</div>

		<div class="timeline-carousel">
			<div class="timeline-carousel-container container">
				<div class="swiper-wrapper">
					<?php if( have_rows('timeline') ):
						while( have_rows('timeline') ) : 
							the_row(); ?>
							<div class="swiper-slide">
								<div class="timeline-icon">
									<img src="<?= get_sub_field('timeline_icon')['url'] ?>">
								</div>
								<div class="timeline-content">
									<p class="timeline-title"><?= get_sub_field('timeline_title') ?></p>
									<div class="timeline-description"><?= get_sub_field('timeline_description') ?></div>
									<?php if(get_sub_field('timeline_cta')): ?>
										<a class="timeline-cta" href="<?= get_sub_field('timeline_cta')['url'] ?>"><?= get_sub_field('timeline_cta')['title'] ?></a>
									<?php endif; ?>
								</div>
							</div>
						<?php endwhile; 
					endif; ?>
				</div>
				<!-- Add Arrows -->
				<div class="swiper-button-next"></div>
			  <div class="swiper-button-prev"></div>
			</div>
		</div>

	</div>
</div>