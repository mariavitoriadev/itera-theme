<?php 
if(is_category()){
	$title = single_cat_title("", false);
	$description = term_description(get_query_var('cat'));
	$image = get_field('category_image',  'category_' . get_query_var('cat'))['url'];
}else if(is_tag()){
	$title = single_tag_title("", false);
	$term = $wp_query->get_queried_object();
	$description = term_description($term);
	$image = get_field('category_image',  'post_tag_' . $term->term_id)['url'];
}else if(is_search()){
	$title = '"'.get_search_query().'"';
}else if (is_404()) {
	$title = "Página não encontrada";
	$image = get_theme_mod( 'page-404-hero' ); 
}
?>

<section class="archive-hero">
	<div class="container archive-thumbnail negative-container"> 
		<amp-img
		class="hero-image"
		layout="fill"
		src="<?= $image ?>"
		srcset="<?= wp_get_attachment_image_srcset(attachment_url_to_postid($image)) ?>">
		></amp-img>
	</div>
	<div class="archive-content minor-container">
		<h1 class="title"><?= $title ?></h1>
		<div class="description"><?= $description ?></div>
	</div>
</section>