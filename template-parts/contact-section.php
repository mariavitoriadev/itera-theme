<div class="contact-section minor-container">
    <p class="contact-main-text">Converse com nosso time e saiba como transformar seus processos com Inteligência Artificial.</p>
    
    <form
        id="contact-form"
        class="contact-form"
        method="post"
        target="_top"
        action="<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php?action=insert_lead"
    >   
        <div class="contact-form-wrapper" id="contact-form-wrapper">
            <label class="contact-label">
                <input name="email" type="email" placeholder="Seu e-mail corporativo*" id="contact-email" class="contact-email">
                <span class="email-input-missing hide">* Campo Obrigatório</span>
                <span class="email-input-invalid hide">* E-mail Inválido</span>
            </label>

            <input type="hidden" class="list-id" name="list-id" value="4">            
            <input class="contact-submit" type="submit" value="Quero saber mais">
        </div>
          
    </form>
</div>