<amp-animation id="hide-top-header" layout="nodisplay">
  <script type="application/json">
    {
        "selector": ".header",
        "duration": "0.2s",
        "fill": "forwards",
        "keyframes": {"transform": "translateY(-37px)"}
    }
  </script>
</amp-animation>

<amp-animation id="show-top-header" layout="nodisplay">
  <script type="application/json">
    {
        "selector": ".header",
        "duration": "0.2s",
        "fill": "forwards",
        "keyframes": {"transform": "translateY(0)"}
    }
  </script>
</amp-animation>

<amp-position-observer
    intersection-ratios="0"
    on="enter:show-top-header.start;exit:hide-top-header.start"
    layout="nodisplay" target="header-trigger-animation">
</amp-position-observer>
