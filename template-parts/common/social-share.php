<div class="social-share container">
	<p>Compartilhe:</p>
	<div class="social-share-icon">
		<amp-social-share width="30" height="23" type="facebook" class="share-link"  data-param-app_id="<?= get_theme_mod( 'social-networks-facebook-id' )?>" data-param-href="<?= get_permalink() ?>" data-vars-social-url="<?= get_permalink() ?>" data-vars-social-type="facebook">
			<?= file_get_contents(get_template_directory().'/images/facebook-blue.svg') ?>
		</amp-social-share>

		<amp-social-share width="30" height="23" type="linkedin" class="share-link" data-vars-social-url="<?= get_permalink() ?>" data-vars-social-type="linkedin">
			<?= file_get_contents(get_template_directory().'/images/linkedin-blue.svg') ?>
		</amp-social-share>
	</div>
</div>
