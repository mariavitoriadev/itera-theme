<?php
	global $wp_query;
	$max_num_pages = $wp_query->max_num_pages;

	if(is_archive() || is_search()) {
		$total_results = $wp_query->found_posts;

		if(is_tag()) {
			$tag = get_queried_object();
			$tag = $tag->slug;
		}
		elseif(is_category()) {
			$category = get_queried_object_id();
		}
	}
?>

<amp-state id="grid">
	<script type="application/json">
		{
			"category" : "",
			"page" : 1,
			"order" : "date",
			"filter" : false,
			"dynamic" : false
		}
	</script>
</amp-state>

<section class="post-grid">
	<span id="post-grid-anchor" class="scroll-anchor"></span>
	<?php if(is_search() || is_archive()): ?>
		<div class="grid-filter">
			<span> Ordenar por: </span>
			<select on="change:AMP.setState({grid : {order : event.value , page : 1, dynamic : true, filter: true }}),dynamic-post-grid.changeToLayoutContainer()">
				<option selected value="date">Mais recentes</option>
				<option value="views">Mais vistos</option>
				<option value="comments">Mais comentados</option>
			</select>
		<?= file_get_contents( get_template_directory() . '/images/down-arrow.svg'); ?>
	</div>
	<?php endif; ?>
	<div class="static-post-grid" [hidden]="grid.page && grid.dynamic">
		<?php while(have_posts()): the_post();
			get_template_part("template-parts/common/post-card");
		endwhile; wp_reset_postdata(); ?>
	</div>
	<amp-list
		id="dynamic-post-grid"
		class="dynamic-post-grid"
		layout="fixed-height"
		width="auto"
		height="1px"
		src="<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts"
		<?php if(is_tag()): ?>
			[src]="grid.dynamic ? ('<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts&category=' + grid.category + '&page=' + grid.page + '&tag=<?= $tag ?>&order=' + grid.order) : '<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts'"
		<?php elseif(is_category()): ?>
			[src]="grid.dynamic ? ('<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts&category=<?= $category ?>&page=' + grid.page + '&order=' + grid.order) : '<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts'"
		<?php elseif(is_search()): ?>
			[src]="grid.dynamic ? ('<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts&category=' + grid.category + '&page=' + grid.page + '&s=<?= get_search_query() ?>&order='+ grid.order) : '<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts'"
		<?php else: ?>
			[src]="grid.dynamic ? ('<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts&category=' + grid.category + '&page=' + grid.page ) : '<?= strpos(site_url(),'localhost') ? '//localhost' : site_url() ?>/wp-admin/admin-ajax.php?action=get_grid_posts'"
		<?php endif; ?>
		>	

		<template type="amp-mustache">
			<?php get_template_part("template-parts/common/dynamic-post-card"); ?>
		</template>
	</amp-list>
	
	<?php if($wp_query->found_posts > 4): ?> 
		<?php get_template_part("template-parts/common/grid-pagination"); ?>
	<?php endif; ?>
</section>
