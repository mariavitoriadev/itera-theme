<div class="post-card">

	<div class="post-thumbnail">
		<a class="post-img" href="<?php the_permalink() ?>">
			<?php the_post_thumbnail('medium'); ?>
			<div class="img-layer"></div>
		</a>
		<a href="<?php the_permalink() ?>">
			<h3 class="post-title">
				<?php the_title(); ?></a>
			</h3>
		</a>
		<p class="post-category">
			<a href="<?= get_category_link(get_the_category()[0]->term_id) ?>">
			<?= get_the_category()[0]->name; ?>
			</a>	
		</p>		
	</div>

	<div class="post-content">
		<div class="post-category-wrapper">
			<p class="post-category">
				<a href="<?= get_category_link(get_the_category()[0]->term_id) ?>">
					<?= get_the_category()[0]->name; ?>	
				</a>
			</p>
			<p class="desktop">Publicado em <?= get_the_date('d/m/Y'); ?></p>

		</div>		
		<a href="<?php the_permalink() ?>">
			<h3 class="post-title">
				<?php the_title(); ?>
			</h3>
		</a>
		<p><?= get_the_excerpt(); ?></p>
		<a href="<?php the_permalink() ?>" class="show-more">Leia mais</a>
	</div>

</div>