<?php $solutions_terms = get_the_terms(get_the_ID(),'solution-category'); ?>
<div class="solution-card">
	<div class="solution-thumbnail">
		<a class="solution-img" href="<?php the_permalink() ?>">
			<?php the_post_thumbnail('medium'); ?>
			<div class="img-layer"></div>
		</a>
		<a href="<?php the_permalink() ?>">
			<h3 class="solution-title">
				<?php the_title(); ?></a>
			</h3>
		</a>
		<?php if(!empty($solutions_terms)): ?>
		<p class="solution-category">
			<a href="<?= get_term_link($solutions_terms[0]->term_id) ?>">
			<?= $solutions_terms[0]->name; ?>	
			</a>	
		</p>
		<?php endif; ?>		
	</div>

	<div class="solution-content">
		<a href="<?php the_permalink() ?>">
			<h3 class="solution-title">
				<?php the_title(); ?>
			</h3>
		</a>
		<div class="solution-category-wrapper">
			<?php if(!empty($solutions_terms)): ?>
			<p class="solution-category">
				<a href="<?= get_term_link($solutions_terms[0]->term_id) ?>">
					<?= $solutions_terms[0]->name; ?>	
				</a>
			</p>
			<?php endif; ?>		
		</div>		
		<p><?= get_field('solution_description') ?></p>
		<a href="<?php the_permalink() ?>" class="show-more">Saiba mais</a>
	</div>

</div>