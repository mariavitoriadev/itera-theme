<div class="grid-pagination">
	<div class="grid-pagination-arrow prev" hidden [hidden]="grid.page < 2" tabindex="1" role="button" on="tap:post-grid-anchor.scrollTo(duration=500),AMP.setState({grid: {page : grid.page - 1, dynamic : true}}),dynamic-post-grid.changeToLayoutContainer()">
		<div class="grid-pagination-link-wrapper">
			<span class="grid-pagination-link"></span>
		</div>
	</div>
	<div class="grid-pagination-numbers">
		<?php 
		global $wp_query;
		$max_num_pages = $wp_query->max_num_pages;

		for($i = 1; $i <= $max_num_pages; $i++){ ?>
			<div <?= $i > 2 && $i != $max_num_pages ? 'hidden' : '' ?> [hidden]="(<?= $i ?> > grid.page + 1 || <?= $i ?> < grid.page - 1) && <?= $i ?> != 1 && <?= $i ?> != <?= $max_num_pages ?>" class="grid-pagination-link-wrapper <?= $i == 1 ? 'current' : '' ?>" [class]="grid.page == <?= $i ?> ? 'grid-pagination-link-wrapper current' : 'grid-pagination-link-wrapper'"><span class="grid-pagination-link" tabindex="1" role="button" on="tap:post-grid-anchor.scrollTo(duration=500),AMP.setState({grid: {page : <?= $i ?>, dynamic : true}}),dynamic-post-grid.changeToLayoutContainer()" ><?= $i ?></span></div>
			<?php if($i == 1): ?>
				<div hidden [hidden]="grid.page < 4" class="grid-pagination-link-wrapper"><span class="grid-pagination-link" >...</span></div>
			<?php endif; ?>
			<?php if($i == ($max_num_pages-1)): ?>
				<div <?= $max_num_pages < 4 ? 'hidden' : '' ?> [hidden]="grid.page > <?= ($max_num_pages-3) ?>" class="grid-pagination-link-wrapper"><span class="grid-pagination-link" >...</span></div>
			<?php endif; ?>
		<?php } ?>
	</div>
	<div class="grid-pagination-arrow next"<?=($max_num_pages == 1) ? ' hidden' : '' ?> [hidden]="grid.page == <?= $max_num_pages ?>" tabindex="1" role="button" on="tap:post-grid-anchor.scrollTo(duration=500),AMP.setState({grid: {page : grid.page + 1, dynamic : true}}),dynamic-post-grid.changeToLayoutContainer()">
		<div class="grid-pagination-link-wrapper">
			<span class="grid-pagination-link"> Próxima Página </span>
		</div>
	</div>
</div>
