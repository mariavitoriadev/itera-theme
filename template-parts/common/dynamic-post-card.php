<div class="post-card">

	<div class="post-thumbnail">
		<a class="post-img" href="{{post_link}}">
			<amp-img
				layout="intrinsic"
				width="{{image_width}}"
				height="{{image_height}}"
				src="{{image_url}}"
				srcset="{{image_srcset}}"
				>
			</amp-img>
			<div class="img-layer"></div>		
		</a>
		<a href="{{post_link}}" >
			<h3 class="post-title">
				{{post_title}}
			</h3>
		</a>
		<p class="post-category">
			<a href="{{category_link}}">
			{{{category_name}}}
			</a>
		</p>
	</div>

	<div class="post-content">
		<div class="post-category-wrapper">
			<p class="post-category">
				<a href="{{category_link}}">
					{{{category_name}}}
				</a>
			</p>
			<p class="post-date desktop">Publicado em <?= get_the_date( 'd/m/Y' ) ?></p>
		</div>
		<a href="{{post_link}}" >
			<h3 class="post-title">
				{{post_title}}
			</h3>
		</a>
		<p class="post-excerpt">{{post_excerpt}}</p>
		<a href="{{post_link}}" class="show-more">Leia mais</a>
	</div>

</div>