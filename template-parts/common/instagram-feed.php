<section class="instagram-feed">
	<div class="container">
		<amp-carousel
		class="instagram-feed-carousel"
		type="carousel"
		layout="fixed-height"
		height="600"
		controls >
		</amp-carousel>
	</div>
</section>
