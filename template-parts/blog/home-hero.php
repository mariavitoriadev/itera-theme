<div class="blog-home-hero">
	<amp-state id="hero_index" hidden="">
		<script type="application/json">
			{
			  "index": "0"
			}
		</script>
	</amp-state>
	<amp-carousel id="blog-home-hero-carousel" class="blog-home-hero-carousel" type="slides" layout="fixed-height" width="auto" height="440" on="slideChange:AMP.setState({hero_index : { index : event.index }})">
	<?php
	$lasts_posts_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => 3
	);
	$lasts_posts = get_posts($lasts_posts_args);
	foreach ($lasts_posts as $post) : 
		$thumbnail = get_the_post_thumbnail($post->ID , 'full', array('layout' => 'fill'));
		$categories = get_the_category($post->ID);
		?>
		<div class="blog-home-hero-post">
			<amp-img
				layout="fill"
				src="<?= get_the_post_thumbnail_url($post->ID, 'medium') ?>"
				srcset="<?= get_the_post_thumbnail_url($post->ID, 'medium') ?> 400vw, <?= get_the_post_thumbnail_url($post->ID, 'medium_large') ?> 768vw, <?= get_the_post_thumbnail_url($post->ID, 'full') ?>">
				></amp-img>
			<div class="hero-background"></div>
			<a class="post-link" href="<?= get_permalink($post->ID) ?>"></a>
			<div class="hero-post-content minor-container">
				<div class="hero-post-top">
					<?php if (!empty( $categories)): ?>
	     			<a class="hero-post-category" href="<?= get_category_link($categories[0]->term_id) ?>">
	     				<?= $categories[0]->name ?>
	     			</a>
	     			<div class="hero-infos desktop">
		     			<p class="post-date">Publicado em <?= get_the_date( 'd/m/Y' , $post->ID ) ?></p>
		     			<span></span>
						<p>Por <?= get_the_author_meta('display_name') ?></p>
					</div>
	     			<?php endif; ?>
	     		</div>   
				<h3 class="hero-post-title"><?= $post->post_title ?></h3>
				<div class="hero-post-infos">
					<p class="post-date mobile">Publicado em <?= get_the_date( 'd/m/Y' , $post->ID ) ?></p>
					<a class="read-more" href="<?= get_permalink($post->ID) ?>">Leia mais</a>
				</div>
			</div>
		</div>			
	<?php endforeach;?>
	</amp-carousel>
	<div class="carousel-bullets">
		<?php foreach ($lasts_posts as $key => $post) : ?>
			<span class="<?= $key == 0? 'select': '' ?>" [class]="hero_index.index == <?= $key ?> ? 'select' : ''" on="tap:blog-home-hero-carousel.goToSlide(index=<?= $key ?>)" ></span>
		<?php endforeach; ?>
	</div>
</div>