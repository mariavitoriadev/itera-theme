<?php 
$terms = get_terms( 'category', array(
	'orderby' => 'name',
	'order' => 'ASC'
)); ?>
<div class="categories-navigator minor-container">
	<p class="category-title">Navegue pelas categorias:</p>
    <div class="categories-navigator-wrapper">
    <?php foreach($terms as $category): ?>
        <a href="<?= get_term_link($category->term_id) ?>">
            <div class="category-item">
                    <p><?= $category->name ?></p>
            </div>
        </a>
    <?php endforeach; ?>
    </div>
</div>