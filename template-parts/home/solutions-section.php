<div class="solutions-section">
	<span class="section-down-target"></span>
	<div class="section-header minor-container">
		<h2 class="title"><?= get_field('solutions_title') ?></h2>
		<p class="description"><?= get_field('solutions_description') ?></p>
	</div>
	<div class="solutions-section-items container">
	<?php
		$solutions_query = new WP_Query( array( 
			'post_type' => 'solucoes',
			'posts_per_page' => 2,
			'meta_query' => array(
				array(
				'key'     => 'is_solution_case',
				'value'   => true,
				'compare' => '=',
				)
			)
		));  
		while($solutions_query->have_posts()): 
			$solutions_query->the_post();
			get_template_part("template-parts/common/solution-card");
		endwhile; 
		wp_reset_postdata(); 
	?>
	</div>
	<a class="solutions-see-more container" href="<?= get_field('solutions_cta')['url'] ?>"><?= get_field('solutions_cta')['title'] ?></a>
</div>