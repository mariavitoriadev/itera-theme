<section class="alice-section">
	<div class="section-header minor-container">
		<h2 class="title"><?= get_field('alice_title') ?></h2>
		<p class="description"><?= get_field('alice_description') ?></p>
	</div>
	
	<?php 
	$slide_1 = get_field('alice_slide_1');
	$slide_2 = get_field('alice_slide_2');
	$slide_3 = get_field('alice_slide_3');
	?>

	<div class="alice-tabs-container minor-container">
		<div class="tab selected">
			<p class="tab-title"><?= $slide_1['slide_tab'] ?></p>
		</div>
		
		<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M1.75391 1.62305L9.13096 9.0001L1.75391 16.3771" stroke="#7A7A7A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>

		<div class="tab">
			<p class="tab-title"><?= $slide_2['slide_tab'] ?></p>
		</div>
		
		<svg width="11" height="18" viewBox="0 0 11 18" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M1.75391 1.62305L9.13096 9.0001L1.75391 16.3771" stroke="#7A7A7A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		</svg>

		<div class="tab">
			<p class="tab-title"><?= $slide_3['slide_tab'] ?></p>
		</div>
	</div>

	<div class="alice-carousel-progress">
		<span></span>
	</div>

	<div class="alice-carousel">
		<div class="alice-carousel-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide desktop-flex">
					<div>
						<p class="item-tab"><?= $slide_1['slide_tab'] ?></p>
						<h2 class="item-title"><?= $slide_1['slide_title'] ?></h3>
						<p class="item-description">
							<?= $slide_1['slide_description'] ?>
						</p>
						<div class="ctas-wrapper">
							<a class="cta cta-meet" href="<?= $slide_1['slide_cta_1']['url'] ?>"><?= $slide_1['slide_cta_1']['title'] ?></a>
							<a class="cta cta-borded"  href="<?= $slide_1['slide_cta_2']['url'] ?>"><?= $slide_1['slide_cta_2']['title'] ?></a>
						</div>
					</div>
					<div class="woman-icon desktop">
						<img src="<?= $slide_1['slide_image']['url'] ?>">
					</div>
				</div>
				<div class="swiper-slide image-reverse desktop-flex">
					<div>
						<p class="item-tab"><?= $slide_2['slide_tab'] ?></p>
						<h3 class="item-title"><?= $slide_2['slide_title'] ?></h3>
						<p class="item-description"><?= $slide_2['slide_description'] ?></p>
						<a class="cta-borded cta" href="<?= $slide_2['slide_cta']['url'] ?>"><?= $slide_2['slide_cta']['title'] ?></a>
					</div>
					<img class="item-image" src="<?= $slide_2['slide_image']['url'] ?>">
				</div>
				<div class="swiper-slide">
					<div class="desktop-padding">
						<p class="item-tab"><?= $slide_3['slide_tab'] ?></p>
						<h3 class="item-title"><?= $slide_3['slide_title'] ?></h3>
						<div class="item-icons">
							<div class="item-icon">
								<img src="<?= $slide_3['slide_icon_1']['url'] ?>">
							</div>
							<div class="item-icon">
								<img src="<?= $slide_3['slide_icon_2']['url'] ?>">
							</div>
							<div class="item-icon">
								<img src="<?= $slide_3['slide_icon_3']['url'] ?>">
							</div>
						</div>
						<p class="item-description"><?= $slide_3['slide_description'] ?></p>
						<a class="cta-borded cta" href="<?= $slide_3['slide_cta']['url'] ?>"><?= $slide_3['slide_cta']['title'] ?></a>
					</div>
				</div>
			</div>
			<!-- Add Arrows -->
			<div class="swiper-button-next"></div>
		    <div class="swiper-button-prev"></div>
		</div>
	</div>
	
</section>