<section class="datas-section">
	<span class="section-down-target"></span>
	<div class="minor-container section-header">	
		<h2 class="title">Não é somente leitura, <strong> é leitura e inteligência.</strong></h2>
		<p class="description">
			Trabalhamos em parceria com sua operação para oferecer a arquitetura adaptada para suas necessidades, com hierarquia de dados, categorização, métricas e análise de sentimento.
		</p>
	</div>
	<div class="data-content container">
		<div class="data-content-menu" >
			<div class="data-selector mobile">
				<select>
				<?php
					$index = 0;
					if( have_rows('data_item') ):
					    while( have_rows('data_item') ) : 
					    	the_row();
					        $title = get_sub_field('data_title'); ?>
					        <option value="data-item-<?= $index ?>" <?= $index++ == 0 ? 'selected="selected"' : '' ?>><?= $title ?></option>	
					    <?php endwhile; 
					endif;
				?>
				</select>
			</div>
			<img class="image" src="https://itera.com.br/wordpress/wp-content/uploads/2020/08/images.jpg">
			<div class="data-selector desktop">
				<?php
					$index = 0;
					if( have_rows('data_item') ):
					    while( have_rows('data_item') ) : 
					    	the_row();
					        $title = get_sub_field('data_title'); ?>
					        <p class="data-item <?= $index == 0 ? 'selected' : '' ?>" data-item="data-item-<?= $index++ ?>" ><?= $title ?></p>	
					    <?php endwhile; 
					endif;
				?>
			</div>
		</div>
		<div class="data-itens-wrapper">
		    <div class="tab-selector">
		        <div class="tab-item selected" id="data-tab-0">
			        <p>Informação detectada</p>
				</div>
				<div class="tab-item" id="data-tab-1">
		        	<p>Métricas</p>
		    	</div>
		        <div class="tab-item" id="data-tab-2">
		        	<p>Json</p>
		        </div>
			</div>
			<?php
				$index = 0;
				if( have_rows('data_item') ):
				    while( have_rows('data_item') ) : 
				    	the_row(); ?>
						<div class="data-item-wrapper <?= $index > 0 ? '' : 'selected' ?>" id="data-item-<?= $index++ ?>">
							<div class="data-item data-tab-0 selected">
								<?= get_sub_field('data_resume'); ?>
							</div>
							<div class="data-item data-tab-1">
								<?= get_sub_field('data_metrics'); ?>
							</div>
							<div class="data-item data-tab-2">
								<?= get_sub_field('data_json'); ?>
							</div>
						</div>
					<?php endwhile; 
				endif;
			?>
		</div>
	</div>
	<a class="data-see-more container" href="/solucoes">Veja exemplos aplicados no mercado</a>
</section>