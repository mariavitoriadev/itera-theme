<div class="partner-section">
	<div class="section-header minor-container">
		<h2 class="title"><?= get_field('carousel_title') ?></h2>
		<p class="description"><?= get_field('carousel_description') ?></p>
	</div>
	<div class="container swiper-container-wrapper">
		<div class="swiper-container">
		    <div class="swiper-wrapper">
			<?php
				if( have_rows('carousel_itens') ):
					while( have_rows('carousel_itens') ) : 
						the_row();
						$url = get_sub_field('item_icon')['url']; ?>
						<div class="swiper-slide">
							<div class="slide-icon">
								<img src="<?= $url ?>">
							</div>
							<div class="swiper-slide-content">
								<h3 class="title"><?= get_sub_field('item_title'); ?></h3>
								<p class="description">
									<?= get_sub_field('item_description'); ?>
								</p>
							</div>
						</div>
					<?php endwhile; 
				endif;
			?> 
			</div>
		    <!-- Add Pagination -->
		    <div class="swiper-button-next"></div>
		    <div class="swiper-button-prev"></div>
		</div>
	</div>
	<a class="partner-see-more container" href="<?= get_field('carousel_cta')['url'] ?>"><?= get_field('carousel_cta')['title'] ?></a>
</div>