<div class="front-page-hero">
	<span class="page-layers-background" id="page-layers-background">
		<span class="layer-1"></span>
		<span class="layer-2"></span>
		<span class="layer-3"></span>
		<span class="layer-4"></span>
		<span class="layer-5"></span>
		<span class="layer-6"></span>
		<span class="layer-7"></span>
		<span class="layer-8"></span>
		<span class="layer-9"></span>
		<span class="layer-10"></span>
	</span>
	<div class="container front-page-content">
		<h2 class="title"><?= get_field('hero_title') ?></h2>
		<p class="description"><?= get_field('hero_description') ?></p>
		<form class="form-know-more" id="form-know-more" method="POST" action="<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php?action=insert_lead">
			<input type="text" class="email-input" name="email" placeholder="Seu e-mail corporativo*" >
			<input type="submit" class="hero-cta" value="Quero saber mais">
			<span class="email-input-missing hide">* Campo Obrigatório</span>
			<span class="email-input-invalid hide">* E-mail Inválido</span>

			<input type="hidden" class="list-id" name="list-id" value="4">
		</form>
	</div>
	<p class="section-down desktop">Veja todo o potencial de nossas soluções</p>
</div>