<section class="companies-section">
	<div class="section-header minor-container">
		<h2 class="title"><?= get_field('partners_title') ?></h2>
		<p class="description"><?= get_field('partners_description') ?></p>
	</div>
	<div class="companies-partners container">
		<div class="swiper-wrapper">
		<?php
			if( have_rows('partners') ):
			    while( have_rows('partners') ) : 
			    	the_row();
			        $url = get_sub_field('partner_logo')['sizes']['medium']; ?>
			        <div class="swiper-slide">
				        <img src="<?= $url ?>">	
					</div>
			    <?php endwhile; 
			endif;
		?>
		</div>
		<!-- Add Pagination -->
		<div class="swiper-button-next"></div>
		<div class="swiper-button-prev"></div>
	</div>
	<div class="companies-colabs minor-container">
		<?php
			if( have_rows('colabs') ):
			    while( have_rows('colabs') ) : 
			    	the_row();
			        $url = get_sub_field('colab_logo')['sizes']['medium']; ?>
			        <img src="<?= $url ?>">	
			    <?php endwhile; 
			endif;
		?>
	</div>
</section>