<?php

global $wp_query;
$results = $wp_query->found_posts;

?>
<section class="search-hero minor-container">
	<?php if($results > 0): ?>
		<p>Mostrando resultados de busca para </p>
	<?php else: ?>
		<p>Nenhum resultado de busca foi encontrado para </p>
	<?php endif; ?>
	<h1 class="title"><?= get_search_query() ?></h1>
	<p class="results"><?= $results ?> resultados encontrados</p>
</section>