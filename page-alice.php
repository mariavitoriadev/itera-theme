<?php /* Template Name: Alice */ ?>

<?php get_header(); ?>

<div class="common-bar minor-container">
	<div class="breadcrumbs">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
	</div>
</div>

<main class="alice">

	<section class="page-hero">
		<div class="minor-container">
			<h1 class="hero-title">
				<?= get_field('alice_title') ?>
			</h1>
			<p class="hero-description"><?= get_field('alice_description') ?></p>
		</div>
	</section>

	<div class="reading-section-wrapper container">
		<p class="section-down desktop">Saiba como funciona a plataforma</p>
	</div>

	<?php $section_1 = get_field('alice_section_1'); ?>
	<section class="reading-section">
		<span class="section-down-target"></span>
		<div class="section-icons container">
			<?= file_get_contents( get_template_directory().'/images/alice-reading.svg') ?>
			<img src="<?= $section_1['section_image'] ?>">
		</div>
		<div class="minor-container reading-header">
			<p class="section-tab"><?= $section_1['section_tab'] ?></p>
			<h2 class="section-title"><?= $section_1['section_title'] ?></h2>
			<p class="section-description"><?= $section_1['section_description'] ?></p>
		</div>
	</section>
	
	<?php $section_2 = get_field('alice_section_2'); ?>
	<section class="comprehension-section">
		<div class="comprehension-layers container">
			<?= file_get_contents( get_template_directory().'/images/alice-reading-2.svg') ?>
			<span class="layer-absolute">
				<?= file_get_contents( get_template_directory().'/images/alice-reading-3.svg') ?>
			</span>
		</div>
		<div class="minor-container comprehension-header">
			<p class="section-tab"><?= $section_2['section_tab'] ?></p>
			<h2 class="section-title"><?= $section_2['section_title'] ?></h2>
			<p class="section-description"><?= $section_2['section_description'] ?>.</p>
		</div>
		<div class="comprehension-cards minor-container">
			<?php if( have_rows('alice_section_2') ): while( have_rows('alice_section_2') ) : the_row(); 
					if( have_rows('comprehension_cards') ): while( have_rows('comprehension_cards') ) : the_row();?>
					<div class="comprehension-card">
						<div class="comprehension-icon">
							<img src="<?= get_sub_field('comprehension_icon')['url'] ?>">
						</div>
						<div class="comprehension-card-content">
							<h3 class="title"><?= get_sub_field('comprehension_title') ?></h3>
							<div class="description">
								<?= get_sub_field('comprehension_description') ?>
							</div>
						</div>
					</div>
				<?php endwhile; endif; 
			endwhile; endif; ?>			
		</div>
	</section>

	<?php $section_3 = get_field('alice_section_3'); ?>
	<section class="learning-section container">
		<img src="<?= $section_1['section_image'] ?>">
		<div class="minor-container learning-header">
			<p class="section-tab"><?= $section_3['section_tab'] ?></p>
			<h2 class="section-title"><?= $section_3['section_title'] ?></h2>
			<p class="section-description"><?= $section_3['section_description'] ?></p>
		</div>
	</section>

	<?php $section_4 = get_field('alice_section_4'); ?>
	<section class="implementation-section">
		<div class="minor-container implementation-header">
			<p class="section-tab"><?= $section_4['section_tab'] ?></p>
			<h2 class="section-title"><?= $section_4['section_title'] ?></h2>
			<p class="section-description"><?= $section_4['section_description'] ?></p>
		</div>
		<div class="implementation-icons container">
			<div class="item-icon">
				<img src="<?= $section_4['section_icon_1']['url'] ?>">
			</div>
			<div class="item-icon">
				<img src="<?= $section_4['section_icon_2']['url'] ?>">
			</div>
			<div class="item-icon">
				<img src="<?= $section_4['section_icon_3']['url'] ?>">
			</div>
		</div>
	</section>

	<section class="solutions-section">
		<div class="minor-container solutions-header">
			<p class="section-tab"><?= get_field('alice_solutions_tab'); ?></p>
			<h2 class="section-title"><?= get_field('alice_solutions_title'); ?></h2>
			<p class="section-description"><?= get_field('alice_solutions_description'); ?></p>
		</div>
	
		<div class="solutions-filter minor-container">
			<?php 
			$solutions = get_terms(array( 'taxonomy' => 'solution-category', 'hide_empty' => true ));
			foreach ($solutions as $solution): ?>
				<p class="filter-item" id="<?= $solution->term_id ?>"><?= $solution->name ?></p>
			<?php endforeach; ?>
		</div>

        <div class="solutions-posts-wrapper container">
            <div class='solutions-posts'>
            <div class="empty-posts">
				<?= file_get_contents( get_template_directory().'/images/empty-state.svg') ?>
				Parece que você ainda não escolheu nenhum setor. Escolha quais são de seu interesse e veja exemplos reais de nossas soluções.
			</div>
            </div>
        </div>
	</section>
</main>

<?php get_footer(); ?>