<!DOCTYPE html >
<html amp <?php language_attributes(); ?>>

<head>
	<?php wp_head(); ?>
	<!-- AMP Analytics -->
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<meta charset="UTF-8"><meta name="viewport" content="width=device-width">
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager -->
	<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-M4FL43W&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>	
	<!-- End Google Tag Manager -->
	<amp-state id="header">
	 	<script type="application/json">
	 		{
	 			"menu" : false
	 		}
		</script>
	</amp-state>
	<header class="header">
		<div class="container header-wrapper">
			<div class="menu-button mobile" [class]="header.menu == false ? 'menu-button mobile' : 'menu-button mobile hidden'" on="tap:AMP.setState({header: {menu: true}})" tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/menu.svg') ?>
			</div>
			<div class="menu-button close hidden mobile" [class]="header.menu == false ? 'menu-button close mobile hidden' : 'menu-button mobile close'" on="tap:AMP.setState({header: {menu: false}})" tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/menu-close.svg') ?>
			</div>
			<div class="logo">
				<div class="logo">
					<?= is_front_page() ? '<h1>' : '' ?>
					<a href="<?= get_home_url()  ?>">
						<?= get_theme_mod('logo-svg') ?>
					</a>
					<?= is_front_page() ? '</h1>' : '' ?>
				</div>
			</div>
			<div class="contact-button mobile"tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/phone.svg') ?>
			</div>
			<div class="menu-main-wrapper desktop">
				<div class="menu-main desktop">
					<?php wp_nav_menu(array('theme_location' => 'header-main')) ?>
				</div>
				<a class="header-cta desktop" href="<?= get_theme_mod('button-url') ?>"><?= get_theme_mod('button-title') ?></a>
			</div>
		</div>
		<?php if( !get_theme_mod('alternative-header') ): ?>
		<div class="menu-open hidden"  [class]="header.menu == false ? 'menu-open hidden' : 'menu-open'">
			<div class="container menu-nav mobile">
				<?php wp_nav_menu(array('theme_location' => 'header-mobile')) ?>
			</div>
			<div class="solutions-menu">
				<div class="header-card desktop">
					<a href="<?= get_theme_mod('header-card-cta'); ?>">
                        <img src="<?= get_theme_mod('header-card-image'); ?>">
                        <div class="header-card-content">
                            <p class="widget-popular-post-title"><?= get_theme_mod('header-card-title'); ?></p>
                            <p class="read-more"><?= get_theme_mod('header-card-cta-text'); ?></p>
                        </div>
					</a>
				</div>
				<a href="/solucoes/" class="alice-card">
					<div>
						<p class="title mobile">Soluções</p>
						<p class="description">Conheça ALICE, nossa plataforma de Inteligência Artificial</p>
						<span class="read-more">Saiba mais</span>
					</div>
				</a>
			</div>
			<div class="solutions-items">
				<p class="title">Soluções por setor</p>
				<div class="solutions-items-wrapper">
					<?php
					$solutions = get_terms(array( 'hide_empty' => false, 'taxonomy' =>'solution-category'));
					foreach ($solutions as $solution): ?>
						<a class="solution-item" href="<?= get_term_link($solution->term_id) ?>"><?= $solution->name ?></a>
					<?php endforeach;?>
				</div>
			</div>
			<div class="cta-menu mobile">
				<a class="header-cta" href="<?= get_theme_mod('button-url') ?>"><?= get_theme_mod('button-title') ?></a>
			</div>
		</div>
		<?php else: ?>	
		<div class="menu-open alternative hidden"  [class]="header.menu == false ? 'menu-open hidden' : 'menu-open alternative'"> 
			<div class="container menu-nav mobile">
				<?php wp_nav_menu(array('theme_location' => 'header-mobile')) ?>
			</div>
			<div class="solutions-menu">
				<div class="header-card">
					<a href="<?= get_theme_mod('header-card-cta'); ?>">
                        <img src="<?= get_theme_mod('header-card-image'); ?>">
                        <div class="header-card-content">
							<p class="sector"><?= get_theme_mod('header-card-sector'); ?></p>
							<p class="widget-popular-post-title"><?= get_theme_mod('header-card-title'); ?></p>
                            <p class="read-more"><?= get_theme_mod('header-card-cta-text'); ?></p>
                        </div>
					</a>
				</div>
				<a href="/solucoes/" class="alice-card">
					<div>
						<p class="sector">Inteligência artifical</p>
						<p class="description">Conheça ALICE, nossa plataforma de Inteligência Artificial</p>
						<span class="read-more">Saiba mais</span>
					</div>
				</a>
				<div class="header-card desktop">
					<a href="<?= get_theme_mod('header-card-2-cta'); ?>">
                        <img src="<?= get_theme_mod('header-card-2-image'); ?>">
                        <div class="header-card-content">
							<p class="sector"><?= get_theme_mod('header-card-2-sector'); ?></p>
                            <p class="widget-popular-post-title"><?= get_theme_mod('header-card-2-title'); ?></p>
                            <p class="read-more"><?= get_theme_mod('header-card-2-cta-text'); ?></p>
                        </div>
					</a>
				</div>
			</div>
			<div class="cta-menu mobile">
				<a class="header-cta" href="<?= get_theme_mod('button-url') ?>"><?= get_theme_mod('button-title') ?></a>
			</div>
		</div>
		<?php endif; ?>
		<?php if(is_single()): ?>
			<amp-animation id="slide-bar" layout="nodisplay" hidden="">
			  <script type="application/json">
			    {
			    "duration": "3s",
			    "fill": "both",
			    "direction": "alternate",
			    "animations": [
			      {
			        "selector": ".reading-bar",
			        "keyframes": [
			          { "transform": "translateX(-100%)" },
			          { "transform": "translateX(0)" }
			        ]
			      }
			    ]
			  }
			  </script>
			</amp-animation>
			<div class="reading-bar"></div>
		<?php endif; ?>
	</header>
	<div class="common-bar-wrapper">
		<div class="common-bar minor-container">
			<div class="breadcrumbs">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
			</div>
			<form id="top-search-form" action="<?= home_url(); ?>" class="top-search-form desktop">
				<input on="input-throttled:clean-search.show" type="text" id="input-text" class="input-text" name="s" placeholder="Digite para buscar no blog...">
				<input type="submit" class="top-search-button" role="button" tabindex="0" value="">
				<span class="search-svg"><?= file_get_contents( get_template_directory() . '/images/search.svg'); ?></span>
				<span id="clean-search" class="clean-search" hidden on="tap:top-search-form.clear" ><?= file_get_contents( get_template_directory() . '/images/search-close.svg'); ?></span>
			</form>
		</div>
	</div>