<?php get_header('amp'); ?>

<?php 
if(have_posts()): 
	while(have_posts()): 
		the_post();?>
		<main class="single-page">
			<div class="single-hero">
				<div class="minor-container">
					<h1 class="hero-title"><?php the_title() ?></h1>
					<div class="hero-infos">
						<p>Publicado em <?= get_the_date('d/m/Y'); ?></p>
						<span></span>
						<p>Por <?= get_the_author_meta('display_name') ?></p>
					</div>
					<p class="hero-post-excerpt"><?= get_the_excerpt() ?></p>
					<div class="hero-social">
						<div class="hero-social-share">
							<amp-social-share type="facebook" data-param-app_id="<?= get_theme_mod('social-networks-facebook-id') ?>"  class="social-share facebook"></amp-social-share>
							<amp-social-share type="linkedin" class="social-share linkedin"></amp-social-share>
							<amp-social-share type="twitter" class="social-share twitter"></amp-social-share>
						</div>
						<?php if(get_field('pdf_file')): ?>
							<a on="tap:AMP.setState({popup_state: {opened: true}})" class="hero-post-download" target="_blank">Download do artigo</a>
						<?php endif; ?>
						<p class="reading-time"><span><?php echo reading_time() ?> min</span> de leitura</p>
					</div>
				</div>
				<amp-img
				class="hero-image"
				layout="fill"
				src="<?= get_the_post_thumbnail_url() ?>"
				srcset="<?= wp_get_attachment_image_srcset(get_post_thumbnail_id()) ?>">
				></amp-img>
				<div class="overlay"></div>
			</div>
			<div class="post-content-wrapper container">
				<div class="post-content">
					<?php the_content() ?>
					<span class="reading-bar-anchor">
					<amp-position-observer intersection-ratios="0" on="scroll:slide-bar.seekTo(percent=event.percent)" layout="nodisplay" hidden=""></amp-position-observer>
					</span>
				</div>
				<div class="post-next-link">
					<?php previous_post_link('<strong>%link</strong>', '%title'); ?>
				</div> 
			</div>
		</main>
		<div class="post-metas-wrapper minor-container">
			<div class="author-metas">
				<?php echo get_avatar( get_the_author_meta( 'ID' ) , 80 ); ?>
				<div class="author-text">
					<p>Publicado por:<br> 
						<span><?= get_the_author_meta('display_name') ?></span>
					</p>
					<p class="author-description"><?= get_the_author_meta('user_description') ?>	</p>
					<?php if(get_the_author_meta('linkedin')): ?>
						<a class="social-network-icon" href="<?= get_the_author_meta('linkedin') ?>" target="_blank">
						<?= file_get_contents( get_template_directory().'/images/linkedin-fill.svg') ?>
					</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="post-metas">
				<p>Categorias: 
					<?php 
					$categories = get_the_category();
					foreach ($categories as $category): ?>
						<a class="category-item" href="<?= get_category_link($category->term_id)?>">
							<?= $category->name ?>
						</a>
					<?php endforeach; ?>
				</p>
				<p>Tags: 
					<?php 
					$tags = get_the_tags();
					foreach ($tags as $tag): ?>
						<a class="tag-item" href="<?= get_tag_link($tag->term_id)?>">
							<?= $tag->name ?> 
						</a>
					<?php endforeach; ?>
				</p>
			</div>
		</div>
		<?php if(get_field('pdf_file')): ?>
		<amp-state id="popup_state" hidden="">
			<script type="application/json">
				{
				  "opened": false
				}
			</script>
		</amp-state>
		<div class="download-popup container hidden" id="download-popup" [class]="popup_state.opened ? 'download-popup container' : 'download-popup container hidden' ">
			<?= get_the_post_thumbnail() ?>
			<div class="popup-content">
				<p class="tab">Saiba mais</p>
				<p class="title"><?= get_the_title() ?></p>
				<form
				id="popup-form"
				class="download-popup-form newsletter-widget"
				method="POST"
				target="_top"
				action-xhr="<?php bloginfo('url'); ?>/wordpress/wp-admin/admin-ajax.php?action=insert_lead"
				custom-validation-reporting="show-all-on-submit"
				on="submit-success: AMP.navigateTo(url='<?= get_field('pdf_file') ?>', target='_blank', opener=true),popup-form.hide"
				>   
				<div class="newsletter-form-wrapper">
					<label class="newsletter-widget-label">
						<input name="name" type="text" placeholder="Seu nome*" id="popup-name" class="newsletter-widget-email" required="">
						<span class="contact-form-alert-msg hide" visible-when-invalid="valueMissing" validation-for="popup-name">* Campo Obrigatório</span>
					</label>
					<label class="newsletter-widget-label">
						<input name="email" type="email" placeholder="Seu e-mail*" id="popup-email" class="newsletter-widget-email" required="">
						<span class="contact-form-alert-msg hide" visible-when-invalid="valueMissing" validation-for="popup-email">* Campo Obrigatório</span>
						<span class="newsletter-widget-alert-msg hide" visible-when-invalid="typeMismatch" validation-for="popup-email">* E-mail Inválido</span>
					</label>
					<div class="input-checkbox">
						<input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
						<span></span>
						<label for="vehicle1">Quero receber novidades ITERA para meu setor.</label>
					</div>

					<input type="hidden" name="nonce" value="<?= wp_create_nonce('insert_lead') ?>">
					<input type="hidden" class="list-id" name="list-id" value="3">
					<input class="newsletter-widget-submit" type="submit" value="Obtenha acesso">
				</div>              
				</form>
			</div>
			<span class="close-popup" on="tap:AMP.setState({popup_state: {opened: false}})" tabindex="0" role="button">
				<?= file_get_contents( get_template_directory().'/images/menu-close.svg') ?>
			</span>
		</div>
		<div class="popup-overlay hidden" [class]="popup_state.opened ? 'popup-overlay' : 'popup-overlay hidden'" on="tap:AMP.setState({popup_state: {opened: false}})"></div>
		<?php endif; 
	endwhile;
endif;
?>
<div class="sidebar-widget container">
	<?php dynamic_sidebar( 'main-sidebar' ); ?>
</div>
<?php get_footer('amp'); ?>