<?php get_header(); ?>

<?php get_template_part('template-parts/home/hero'); ?>
<?php get_template_part('template-parts/home/solutions-section'); ?>
<?php get_template_part('template-parts/home/partner-section'); ?>
<?php get_template_part('template-parts/home/alice-section'); ?>
<?php get_template_part('template-parts/home/companies-section'); ?>

<div class="sidebar-widget container">
	<?php dynamic_sidebar( 'main-sidebar' ); ?>
</div>
<?php get_footer(); ?>